package organization

class MultipleMessageException extends Exception {

    Collection<String> messages

    void setMessages(Collection<String> messages) {
    }

    MultipleMessageException(Collection<String> msgs) {
        super((msgs != null && msgs.size() > 0) ?
                msgs.iterator().hasNext() ?
                        msgs.iterator().next() : null : null)
        messages = !msgs ? new ArrayList<String>() : msgs
    }

    MultipleMessageException(String message) {
        super(message)
    }
}
