package organization


class RegistrationException extends MultipleMessageException {
    RegistrationException(Collection<String> messages) {
        super(messages)
    }
}
