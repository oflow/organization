package organization

class PasswordException extends MultipleMessageException {
    PasswordException(Collection<String> messages) {
        super(messages)
    }
}
