package organization.service

import organization.RegistrationException
import organization.repository.UserDomain
import organization.model.RegistrationModel

interface IRegistrationService {

    static final String ACTIVATION_KEY_FIELD_NAME = "key"

    UserDomain findUserByPendingKey(String key)

    Boolean isPendingKeyExists(String key)

    void deleteUserByPendingKey(String key)

    void completeRegistration(String key, String header)

    void checkUserUniqueConstraints(UserDomain user) throws RegistrationException

    void register(RegistrationModel registrationModel)

    RegistrationModel initModel()

    void validate(RegistrationModel registrationModel) throws RegistrationException
}

