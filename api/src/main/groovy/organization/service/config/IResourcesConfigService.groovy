package organization.service.config

interface IResourcesConfigService {
    Set<IMailConfigService> getMailConfigServices()

    Set<IDatabaseConfigService> getDatabaseConfigServices()
}