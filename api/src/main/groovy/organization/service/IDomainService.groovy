package organization.service

import organization.repository.Domain

interface IDomainService {
    Domain save(Domain model)

    Domain find(Class<? extends Domain> entityClass, Serializable id)

    void delete(Domain model)

    Integer count(Class<? extends Domain> entityClass)

    List<? extends Domain> findAll(Class<? extends Domain> entityClass)

    List<? extends Domain> findAll(Class<? extends Domain> entityClass, Integer[] range)

    List<? extends Domain> updateAll(Collection<? extends Domain> models)

    List<? extends Domain> saveAll(Collection<? extends Domain> models)

    void deleteAll(Collection<? extends Domain> models)
}

