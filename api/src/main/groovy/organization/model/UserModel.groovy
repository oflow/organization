package organization.model

import organization.repository.EmailDomain
import organization.repository.UserDomain

interface UserModel
<USER extends UserDomain,
EMAIL extends EmailDomain> {

    USER getUser()

    void setUser(USER user)

    List<EMAIL> getEmails()

    void setEmails(List<EMAIL> emails)

    String getSecondaryEmailValue()

    void setSecondaryEmailValue(String secondaryEmailValue)

    String getPassword()

    void setPassword(String password)

    void setSecondaryEmail(EMAIL email)

}