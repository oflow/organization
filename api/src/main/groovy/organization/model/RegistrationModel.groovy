package organization.model

interface RegistrationModel extends UserModel {
    String getUsername()

    void setUsername(String username)

    String getEmail()

    String getEmailRetype()

    String getPasswordRetype()

    String getHeader()

    void setEmail(String email)

    void setEmailRetype(String emailRetype)

    void setPasswordRetype(String passwordRetype)

    void setHeader(String header)
}