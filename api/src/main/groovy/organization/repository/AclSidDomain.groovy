package organization.repository

interface AclSidDomain extends Domain<Long>{
    String getSid()
    void setSid(String sid)
    Boolean getPrincipal()
    void setPrincipal(Boolean principal)
}