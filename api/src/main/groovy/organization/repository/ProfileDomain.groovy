package organization.repository

import organization.GenderTypeEnum


interface ProfileDomain<USER extends UserDomain, LANG extends LangDomain> extends Domain<Long> {
    static final String ID_COLUMN = 'profile_id'
    static final String SIGNATURE_INDEX = 'idx_profile_signature'
    static final String FIRST_NAME_INDEX = 'idx_profile_first_name'
    static final String LAST_NAME_INDEX = 'idx_profile_last_name'
    static final String SECOND_NAME_INDEX = 'idx_profile_second_name'
    static final String GENDER_INDEX = 'idx_profile_gender'
    static final String DATE_OF_BIRTH_INDEX = 'idx_profile_dob'
    static final String DATE_CREATED_INDEX = 'idx_profile_reg_date'


    USER getUser()

    void setUser(USER user)

    LANG getLang()

    void setLang(LANG lang)

    String getSignature()

    void setSignature(String signature)

    String getLastName()

    void setLastName(String lastName)

    String getSecondName()

    void setSecondName(String secondName)

    String getFirstName()

    void setFirstName(String firstName)

    GenderTypeEnum getGender()

    void setGender(GenderTypeEnum gender)

    Date getDateOfBirth()

    void setDateOfBirth(Date dob)

    Date getDateCreated()

    void setDateCreated(Date dateCreated)
}