package organization.repository

interface AclEntryDomain
<ACL_OBJECT_IDENTITY extends AclObjectIdentityDomain,
ACL_SID extends AclSidDomain> extends Domain<Long> {

    ACL_OBJECT_IDENTITY getAclObjectIdentity()

    Integer getAceOrder()

    void setAceOrder(Integer aceOrder)

    ACL_SID getSid()

    void setSid(ACL_SID sid)

    Integer getMask()

    void setMask(Integer mask)

    Boolean getGranting()

    void setGranting(Boolean granting)

    Boolean getAuditSuccess()

    void setAuditSuccess(Boolean auditSuccess)

    Boolean getAuditFailure()

    void setAuditFailure(Boolean auditFailure)
}