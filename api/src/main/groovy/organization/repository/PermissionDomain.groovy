package organization.repository

interface PermissionDomain extends Domain<Long> {
    static final String PERMISSION_VALUE_FIELD_NAME = "permissionValue"

    String getPermissionValue()

    void setPermissionValue(String permissionValue)
}
