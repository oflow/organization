package organization.repository

interface UserEmailDomain<USER extends UserDomain, EMAIL extends EmailDomain> extends Domain<Long> {
    Boolean getPrimaryEmail()

    void setPrimaryEmail(Boolean primaryEmail)

    Date getEndDate()

    void setEndDate(Date endDate)

    Date getStartDate()

    void setStartDate(Date startDate)

    USER getUser()

    void setUser(USER user)

    EMAIL getEmail()

    void setEmail(EMAIL email)
}
