package organization.repository.dao

import organization.repository.PropertyDomain

interface IPropertyDao<M extends PropertyDomain, ID> extends Dao<M, ID> {

    //    def getValueByKey(String key)
    //    def findByKeyLike(String keyLike)
    PropertyDomain findByKey(String key)

    PropertyDomain createOrUpdate(String key, String value)

    PropertyDomain createOrUpdate(String key, String value, Boolean updated)
}