package organization.repository.dao

import organization.repository.Domain

interface Dao<M extends Domain, ID> {
     M save(M model)

     M find(ID id)

     void delete(M model)

     Integer count()

     List<M> findAll()

     List<M> findAll(Integer[] range)

     List<M> updateAll(Collection<M> models)

     List<M> saveAll(Collection<M> models)

     void deleteAll(Collection<M> models)
}
