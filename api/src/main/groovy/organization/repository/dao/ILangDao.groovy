package organization.repository.dao

import organization.repository.LangDomain

interface ILangDao<M extends LangDomain, ID> extends Dao<M, ID> {
    LangDomain findByCode(String code)

    LangDomain addSupportedLang(String code)
}
