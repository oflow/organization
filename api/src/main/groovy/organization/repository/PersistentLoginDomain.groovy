package organization.repository

interface PersistentLoginDomain extends Domain<String> {
    String getUsername()

    void setUsername(String username)

    String getToken()

    void setToken(String token)

    Date getLastUsed()

    void setLastUsed(Date lastUsed)

    String getSeries()

    void setSeries(String series)
}