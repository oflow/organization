package organization.repository

interface Domain<ID> {
    ID getId()

    void setId(ID id)

    ID parseId(String value)
}
