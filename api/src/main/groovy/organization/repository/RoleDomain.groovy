package organization.repository

interface RoleDomain<USER_ROLE extends UserRoleDomain> extends Domain<Long> {
    static final String AUTHORITY_FIELD_NAME = 'authority'

    String getAuthority()

    void setAuthority(String authority)

    void toUpperCase()

    void toLowerCase()

    Set<USER_ROLE> getUserRoles()

    void setUserRoles(Set<USER_ROLE> userRoles)
}