package organization.repository

interface UserEnabledDomain<USER extends UserDomain> extends Domain<Long> {

    Boolean getEnabled()

    void setEnabled(Boolean enabled)

    Date getStartDate()

    void setStartDate(Date startDate)

    Date getEndDate()

    void setEndDate(Date endDate)

    USER getUser()

    void setUser(USER user)

    USER getUserGranter()

    void setUserGranter(USER userGranter)

    USER getUserRevoker()

    void setUserRevoker(USER userRevoker)
}