package organization.repository

interface GroupDomain<ROLE extends RoleDomain, USER extends UserDomain> extends Domain<Long> {
    static final String FIND_BY_GROUP_NAME = "Group.findByGroupName"

    String getGroupName()

    void setGroupName(String groupName)

    Set<ROLE> getRoles()

    void setRoles(Set<ROLE> role)

    Set<USER> getUsers()

    void setUsers(Set<USER> users)
}