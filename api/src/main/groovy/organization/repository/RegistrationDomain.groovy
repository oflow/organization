package organization.repository

interface RegistrationDomain extends Domain<Long> {
    String getUsername()

    void setUsername(String username)

    String getToken()

    void setToken(String token)

    Date getDateCreated()

    void setDateCreated(Date dateCreated)
}