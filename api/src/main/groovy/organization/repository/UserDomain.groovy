package organization.repository

interface UserDomain
<PROFILE extends ProfileDomain,
PASSWORD extends PasswordDomain,
USER_ROLE extends UserRoleDomain,
USER_EMAIL extends UserEmailDomain,
USER_ENABLED extends UserEnabledDomain,
USER_ACCESS extends UserAccessDomain> extends Domain<Long> {
    static final String USERNAME_FIELD_NAME = 'username'
    static final String CURRENT_EMAIL_VALUE_FIELD_NAME = 'currentEmailValue'
    static final String PASSWORD_FIELD_NAME = 'password'
    static final String[] EXCLUDES = [
            "id", "version", "password", "profile",
            "userRoles", "userEnableds", "userAccesses",
            "userEmails", "passwords"
    ]

    String getEmail()

    void setEmail(String email)

    String getUsername()

    void setUsername(String username)

    String getPassword()

    void setPassword(String password)

    Boolean getEnabled()

    void setEnabled(Boolean enabled)

    Boolean getAccountExpired()

    void setAccountExpired(Boolean accountExpired)

    Boolean getAccountLocked()

    void setAccountLocked(Boolean accountLocked)

    Boolean getPasswordExpired()

    void setPasswordExpired(Boolean passwordExpired)

    Set<USER_ROLE> getUserRoles()

    void setUserRoles(Set<USER_ROLE> userRoles)

    Set<USER_EMAIL> getUserEmails()

    void setUserEmails(Set<USER_EMAIL> userEmails)

    Set<PASSWORD> getPasswords()

    void setPasswords(Set<PASSWORD> passwords)

    PROFILE getProfile()

    void setProfile(PROFILE profile)

    Set<USER_ENABLED> getUserEnableds()

    void setUserEnableds(Set<USER_ENABLED> userEnableds)

    Set<USER_ACCESS> getUserAccesses()

    void setUserAccesses(Set<USER_ACCESS> userAccesses)
}
