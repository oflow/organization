package organization.repository

interface RequestMapDomain extends Domain<Long> {
    String getConfigAttribute()

    void setConfigAttribute(String configAttribute)

    String getUrl()

    void setUrl(String url)
}