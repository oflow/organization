package organization.repository


interface UserRoleDomain<USER extends UserDomain, ROLE extends RoleDomain> extends Domain<Long> {

    USER getUser()

    void setUser(USER user)

    ROLE getRole()

    void setRole(ROLE role)
}