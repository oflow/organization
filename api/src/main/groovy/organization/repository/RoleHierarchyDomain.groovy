package organization.repository

interface RoleHierarchyDomain<ROLE extends RoleDomain> extends Domain<Long>{
    static final String FIND_BY_RANK = "RoleHierarchy.findByRank"
    static final String FIND_BY_ROLE = "RoleHierarchy.findByRole"
    static final String ROLE_FIELD_NAME = "role"

    Integer getRank()

    void setRank(Integer rank)

    ROLE getRole()

    void setRole(ROLE role)
}