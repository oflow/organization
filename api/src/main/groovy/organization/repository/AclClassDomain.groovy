package organization.repository

interface AclClassDomain extends Domain<Long> {
    String getClassName()

    void setClassName(String className)
}