package organization.repository

interface UserAccessDomain<USER extends UserDomain, HTTP_HEADER extends HttpHeaderDomain> extends Domain<Long> {

    Date getDate()

    void setDate(Date date)

    HTTP_HEADER getHttpHeader()

    void setHttpHeader(HTTP_HEADER httpHeader)

    USER getUser()

    void setUser(USER user)
}