package organization.repository

interface HttpHeaderDomain<BROWSER extends BrowserDomain> extends Domain<Long> {
    BROWSER getBrowser()

    void setBrowser(BROWSER browser)

    String getHeader()

    void setHeader(String header)

    Date getDate()

    void setDate(Date date)

    String getAccept()

    void setAccept(String accept)

    String getAcceptCharset()

    void setAcceptCharset(String acceptCharset)

    String getAcceptEncoding()

    void setAcceptEncoding(String acceptEncoding)

    String getAcceptLanguage()

    void setAcceptLanguage(String acceptLanguage)

    String getCacheControl()

    void setCacheControl(String cacheControl)

    String getConnection()

    void setConnection(String connection)

    String getCookie()

    void setCookie(String cookie)

    String getHost()

    void setHost(String host)

    String getIpv4address()

    void setIpv4address(String ipv4address)

    String getIpv6address()

    void setIpv6address(String ipv6address)

    String getIsp()

    void setIsp(String isp)

    String getKeepAlive()

    void setKeepAlive(String keepAlive)

    String getProtocol()

    void setProtocol(String protocol)

    String getReferer()

    void setReferer(String referer)

    String getUri()

    void setUri(String uri)

    String getUserAgent()

    void setUserAgent(String userAgent)

}