package organization.repository

interface LangDomain extends Domain<Long> {

    static final String LANG_CODE_FIELD_NAME = 'code'
    static final String ID_COLUMN = 'lang_id'
    static final String SUPPORTED_INDEX = 'idx_lang_supported'
    static final String ID_FIELD = 'id'
    static final String CODE_FIELD = 'code'
    static final String SUPPORTED_FIELD = 'supported'

    String getCode()

    void setCode(String code)

    Boolean getSupported()

    void setSupported(Boolean supported)
}