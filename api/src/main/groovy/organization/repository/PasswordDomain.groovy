package organization.repository

interface PasswordDomain<USER extends UserDomain> extends Domain<Long> {
    String getPasswordValue()

    void setPasswordValue(String passwordValue)

    Date getStartDate()

    void setStartDate(Date startDate)

    Date getEndDate()

    void setEndDate(Date endDate)

    USER getUser()

    void setUser(USER user)
}