package organization.repository

interface PropertyDomain extends Domain<Long>{
/*=================================*/
    static final String KEY_FIELD_NAME = 'key'
    static final String VALUE_FIELD_NAME = 'value'
    static final String ID_COLUMN = 'property_id'
    static final String KEY_COLUMN = 'property_key'
    static final String VALUE_COLUMN = 'property_value'
    static final String KEY_FIELD = 'key'
    static final String VALUE_FIELD = 'value'
/*=================================*/

    String getKey()

    void setKey(String key)

    String getValue()

    void setValue(String value)

}