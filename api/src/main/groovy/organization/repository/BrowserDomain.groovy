package organization.repository

interface BrowserDomain<HTTP_HEADER extends HttpHeaderDomain> extends Domain<Long> {
     static final String VERSION_FIELD = "browserVersion"
     static final String NAME_FIELD = "browserName"
     static final String OS_FIELD = "browserOs"
     static final String ID_COLUMN = "browser_id"

      String getBrowserVersion()

     void setBrowserVersion(String browserVersion)

     String getBrowserName()

     void setBrowserName(String browserName)

     String getBrowserOs()

     void setBrowserOs(String browserOs)

     Set<HTTP_HEADER> getHeaders()

     void setHeaders(Set<HTTP_HEADER> headers)
}
