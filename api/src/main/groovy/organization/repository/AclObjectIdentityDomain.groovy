package organization.repository

interface AclObjectIdentityDomain
<ACL_OBJECT_IDENTITY extends AclObjectIdentityDomain,
ACL_CLASS extends AclClassDomain,
ACL_SID extends AclSidDomain> extends Domain<Long> {
    Long getObjectId()

    void setObjectId(Long objectId)

    ACL_CLASS getAclClass()

    void setAclClass(ACL_CLASS aclClass)

    ACL_OBJECT_IDENTITY getParent()

    void setParent(ACL_OBJECT_IDENTITY Parent)

    AclSidDomain getOwner()

    void setOwner(ACL_SID owner)

    Boolean getEntriesInheriting()

    void setEntriesInheriting(Boolean entriesInheriting)
}