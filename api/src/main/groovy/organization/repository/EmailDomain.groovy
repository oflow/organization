package organization.repository

interface EmailDomain<USER_EMAIL extends UserEmailDomain> extends Domain<Long> {
    static final String EMAIL_VALUE_FIELD_NAME = 'emailValue'
    static final String ID_COLUMN = 'email_id'

    String getEmailValue()

    void setEmailValue(String authority)

    void emailValueToLowerCase()

    Set<USER_EMAIL> getUserEmails()

    void setUserEmails(Set<USER_EMAIL> userEmails)
}