package organization

enum LoggerLevelEnum {
   TRACE, DEBUG, INFO, WARN, ERROR
}
