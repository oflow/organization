package organization

enum AuthoritiesEnum {
   ROLE_ADMIN, ROLE_MANAGER, ROLE_USER, ROLE_GUEST
}
