package organization.context

import org.springframework.context.ApplicationContext
import organization.model.config.IApplicationConfigModel

import javax.servlet.ServletContext

abstract class AbstractWebApplicationContextLoaderHelper<AC extends ApplicationContext, ACM extends IApplicationConfigModel> {
    Map applicationContextMap
    AC applicationContext
    ACM applicationConfigModel
    ConfigObject configObject
    ServletContext servletContext

    AC load() {
        loadBeansDefinition()
        loadConfiguration()
        loadServices()
        loadMvc()
//        TODO : uncomment when ready, scan only when context populated
//        loadBasePackages()
    }

    abstract AC loadBeansDefinition()

    abstract AC loadConfiguration()

    abstract AC loadServices()

    abstract AC loadMvc()

    abstract AC loadBasePackages()
}
