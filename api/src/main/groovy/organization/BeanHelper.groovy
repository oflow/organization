package organization

class BeanHelper {
    static String simpleInstanceName(Class clazz) {
        def sb = new StringBuilder(clazz.simpleName)
        sb.replace 0, 1, new String(sb.toString().charAt(0)).toLowerCase()
        sb.toString()
    }
}
