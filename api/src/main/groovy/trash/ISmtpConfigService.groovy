package trash

interface ISmtpConfigService extends IConfigService{
    def update(Map<String, Object> map)

    //TODO: externalize all of those keys
    static final String CONTENT_TYPE_HTML_KEY = "mail.content.type"
    static final String CONTENT_TYPE_HTML_VALUE = "text/html; charset=UTF-8"
    static final String JNDI_SESSION_NAME = "mail/noreply"
    static final String MAIL_SMTP_HOST_KEY = "mail.smtp.host"
    static final String MAIL_SMTP_PORT_KEY = "mail.smtp.port"
    static final Integer MAIL_SMTP_PORT_DEFAULT_VALUE = 25
    static final String MAIL_SMTP_USER_KEY = "mail.smtp.user"
    static final String MAIL_SMTP_PASSWORD_KEY = "mail.smtp.password"
    static final String MAIL_SMTP_FROM_KEY = "mail.smtp.from"
    static final String MAIL_SMTP_STARTTLS_PORT_KEY = "mail.smtp.tls.starttls.port"
    static final String MAIL_SMTP_STARTTLS_ENABLE_KEY = "mail.smtp.tls.starttls.enabled"
}