package trash

interface ICoreApplicationConfigService {

    def load()

    ConfigObject getApplicationConfig()
}
