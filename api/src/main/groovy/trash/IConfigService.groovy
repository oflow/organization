package trash

import org.springframework.context.ApplicationContextAware

interface IConfigService extends ICoreApplicationConfigService, ApplicationContextAware {


    void setApplicationConfig(ConfigObject configObject)
}
