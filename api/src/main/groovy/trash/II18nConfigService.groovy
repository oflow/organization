package trash

interface II18nConfigService extends IConfigService{
    //LANG
    static final String BUNDLE_BASE_NAME_VALUE = "i18n.messages"
    static final String LANG_CODE_EN = Locale.ENGLISH.language
    static final String LANG_CODE_FR = Locale.FRENCH.language
    static final String LANG_CODE_DEFAULT = LANG_CODE_EN
}