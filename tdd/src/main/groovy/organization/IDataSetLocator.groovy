package organization

interface IDataSetLocator {
    def getDataSet()
}
