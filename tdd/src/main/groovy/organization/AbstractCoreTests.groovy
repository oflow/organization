package organization

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Value
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.test.context.transaction.TransactionConfiguration
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.support.DefaultTransactionDefinition

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@RunWith(SpringJUnit4ClassRunner)
@TransactionConfiguration
@TestExecutionListeners([CleanDataInsertTestExecutionListener, DependencyInjectionTestExecutionListener])
abstract class AbstractCoreTests extends AbstractTransactionalJUnit4SpringContextTests implements IDataSetLocator {

    @Value('${database.dataSet}')
    String dataSet
    @PersistenceContext
    EntityManager em

    @Transactional(readOnly = true)
    def countEntity(Class<?> clazz) {
        em.createQuery("select count(*) from ${clazz.simpleName}", Long).singleResult.intValue()
    }

    TransactionStatus getTransactionStatus() {
        applicationContext.getBean(PlatformTransactionManager).getTransaction new DefaultTransactionDefinition()
    }
    /**
     * Flush the underlying session to the datastore, if applicable:
     * for example, all affected Hibernate/JPA sessions.
     */
    def flush() {
        applicationContext.getBean(JpaTransactionManager).getTransaction(new DefaultTransactionDefinition()).flush()
    }
}
