package organization

import org.dbunit.DatabaseUnitException
import org.dbunit.database.DatabaseConfig
import org.dbunit.database.DatabaseDataSourceConnection
import org.dbunit.database.IDatabaseConnection
import org.dbunit.dataset.DataSetException
import org.dbunit.dataset.IDataSet
import org.dbunit.dataset.ITable
import org.dbunit.dataset.datatype.DefaultDataTypeFactory
import org.dbunit.dataset.filter.DefaultColumnFilter
import org.dbunit.dataset.xml.FlatXmlDataSet
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder
import org.dbunit.ext.h2.H2DataTypeFactory
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory
import org.dbunit.ext.mysql.MySqlDataTypeFactory
import org.dbunit.ext.oracle.OracleDataTypeFactory
import org.dbunit.ext.postgresql.PostgresqlDataTypeFactory
import org.dbunit.operation.DatabaseOperation

import javax.inject.Inject
import javax.inject.Named
import javax.sql.DataSource
import java.sql.SQLException

class DbUnitHelper {

    static final String VERIFICATION_EXCEPTION = "Error during verification"

    /**
     *  Retrieve a dataset.
     * @param dataSetResourcePath
     * @return
     * @throws Exception
     */
    static IDataSet getDataSet(String dataSetResourcePath) throws Exception {
        new FlatXmlDataSetBuilder(columnSensing: true)
                .build(new File(dataSetResourcePath))
    }

    @Inject
    @Named("dataSource")
    DataSource dataSource

    IDatabaseConnection getConnection() throws Exception {
        DatabaseDataSourceConnection dataSourceConnection =
                new DatabaseDataSourceConnection(dataSource)
        // avoid the type not recognized problem
        // http://dbunit.sourceforge.net/faq.html#typenotrecognized
        DatabaseConfig config = dataSourceConnection.config
        String databaseType = dataSource.connection.metaData.databaseProductName
        switch (databaseType) {
            case "MySQL":
                config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                        new MySqlDataTypeFactory())
                break
            case "H2":
                config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                        new H2DataTypeFactory())
                break
            case "HSQL Database Engine":
                config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                        new HsqldbDataTypeFactory())
                break
            case "PostgreSQL":
                config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                        new PostgresqlDataTypeFactory())
                break
        //recheck this case refere this url http://code.google.com/p/rimudb/source/browse/trunk/rimudb/src/org/rimudb/sql/SQLAdapterFactory.java?spec=svn464&r=464
            case "Oracle":
                config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                        new OracleDataTypeFactory())
                break
        //case "Apache Derby":
            default: config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new DefaultDataTypeFactory())
        }
        dataSourceConnection
    }

    /**
     * Do a clean insert.
     *
     * @param dataSet
     */
    void doCleanInsert(IDataSet dataSet) {
        try {
            DatabaseOperation.CLEAN_INSERT.execute connection, dataSet
        } catch (Exception e) {
            throw new RuntimeException(
                    "Error while doing a clean insert : " + e.message, e)
        }
    }

    void doSafeDelete(IDataSet dataSet) {
        try {
            DatabaseOperation.DELETE_ALL.execute connection, dataSet
        }
        catch (Exception e) {
            throw new RuntimeException("Error while doing a safe delete : " + e.message, e)
        }
    }

    /**
     * Table content and dataset comparison. Comparison is based on the dataset
     * columns.
     *
     * @param tableName
     * @param dataSet
     */
    void compareTables(String tableName, IDataSet dataSet) {
        try {
            ITable currentTable = connection.createDataSet().getTable(tableName)
            // what we are expecting (from the dataset)
            ITable expectedTable = dataSet.getTable(tableName)
            // filter from the dataset content
            currentTable = DefaultColumnFilter.includedColumnsTable(
                    currentTable,
                    expectedTable.tableMetaData.columns)
            // assertion
            assert expectedTable == currentTable
        } catch (DataSetException dse) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, dse)
        } catch (SQLException se) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, se)
        } catch (DatabaseUnitException due) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, due)
        } catch (Exception e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        }
    }

    /**
     * Check row count in a table.
     *
     * @param tableName
     * @param expectedCount
     */
    void compareTableCount(String tableName, Integer expectedCount) {
        try {
            assert expectedCount == connection.createDataSet().getTable(tableName).rowCount
        } catch (DataSetException dse) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, dse)
        } catch (SQLException se) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, se)
        } catch (Exception e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        }
    }

    /**
     * Table content and dataset comparison. Comparison is based on the dataset
     * columns.
     *
     * @param tableName
     * @param ressourceDataSet
     */
    void compareTables(String tableName, String ressourceDataSet) {
        FlatXmlDataSet dataSet
        try {
            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder()
            builder.columnSensing = true
            dataSet = builder.build(new File(ressourceDataSet))
        } catch (DataSetException dse) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, dse)
        } catch (IOException e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        }
        compareTables(tableName, dataSet)
    }

    /**
     * Retrieve a table from the database.
     *
     * @param tableName
     * @return
     */
    ITable getTable(String tableName) {
        try {
            connection.createDataSet().getTable(tableName)
        } catch (DataSetException e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        } catch (SQLException e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        } catch (Exception e) {
            throw new RuntimeException(VERIFICATION_EXCEPTION, e)
        }
    }
}
