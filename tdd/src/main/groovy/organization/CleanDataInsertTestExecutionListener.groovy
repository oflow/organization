package organization

import groovy.util.logging.Slf4j
import org.springframework.test.context.TestContext
import org.springframework.test.context.TestExecutionListener

import javax.sql.DataSource

@Slf4j
class CleanDataInsertTestExecutionListener implements TestExecutionListener {

    /**
     * Does data injection.
     */
    @Override
    void beforeTestMethod(TestContext testContext) throws Exception {
        if (testContext.testInstance instanceof IDataSetLocator) {
            IDataSetLocator test = testContext.testInstance as IDataSetLocator
            DbUnitHelper helper
            // a DbUnit helper in the context?
            if (testContext.applicationContext.containsBean("dbUnitHelper")) {
                // yes, use it
                helper = testContext.applicationContext.getBean("dbUnitHelper", DbUnitHelper)
            } else {
                // no, create one
                helper = new DbUnitHelper(
                    dataSource: testContext.applicationContext.getBean("dataSource", DataSource))

            }
            // do the clean insert
            //log.info("doing clean insert from {} for method {}",
            //      test.dataSet,
            //    testContext.testMethod.name)
            helper.doCleanInsert(helper.getDataSet(test.dataSet as String))
        } else {
            //log.info("{} n'est pas une classe contenant un dataset, pas d'injection de données",
            //      testContext.class.name)
        }
    }

    @Override
    void beforeTestClass(TestContext testContext) throws Exception {
    }

    @Override
    void afterTestClass(TestContext testContext) throws Exception {
    }

    @Override
    void prepareTestInstance(TestContext testContext) throws Exception {
    }

    @Override
    void afterTestMethod(TestContext testContext) throws Exception {
        if (testContext.testInstance instanceof IDataSetLocator) {
            IDataSetLocator test = (IDataSetLocator) testContext.testInstance
            DbUnitHelper helper
            // a DbUnit helper in the context?
            if (testContext.applicationContext.containsBean("dbUnitHelper")) {
                // yes, use it
                helper = testContext.applicationContext.getBean("dbUnitHelper", DbUnitHelper)
            } else {
                // no, create one
                helper = new DbUnitHelper(
                    dataSource: testContext.applicationContext.getBean("dataSource", DataSource))
            }
            // do the clean dump
            //log.info("dumping tables from ${test.dataSet} for method ${testContext.testMethod.name}")
            helper.doSafeDelete(helper.getDataSet(test.dataSet as String))
        } else {
            log.debug "${testContext.class.name} is not a class containing a dataset, no data been injected"
        }
    }
}
