package toolbox

class Calculator {
    def stack = [] as ArrayList<Double>

    void push(Double arg) { stack.add arg }

    Double divide() { stack.get(0) / stack.get(1) }
}
