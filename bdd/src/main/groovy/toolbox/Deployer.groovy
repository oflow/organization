package toolbox

import static org.slf4j.LoggerFactory.getLogger

class Deployer {
    static log = getLogger(Deployer)

    static Boolean isWarExists() { null }

    static def main(args) {
        log.info "Hello from ${Deployer.simpleName}"
    }
}