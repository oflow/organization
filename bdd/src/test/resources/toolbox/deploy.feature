# language: en
Feature: Deploy Application
  take a war and deploy it on localhost in an embedded mode

  Scenario: Web App Spring
    Given I have the war file
    When tomcatRunWar
    And the application is ready
    Then open home page

  Scenario: Web App Grails
    Given I have the war file
    When run-app
    And the application is ready
    Then open home page


