package toolbox

this.metaClass.mixin(cucumber.api.groovy.Hooks)

class CustomWorld {
    String customMethod() {
        "foo"
    }
}

World {
    new CustomWorld()
}

Before() {
    assert "foo" == customMethod()
    calc = new Calculator()
}

Before("@notused") {
    throw new RuntimeException("Never happens")
}

Before("@notused,@important", "@alsonotused") {
    throw new RuntimeException("Never happens")
}
