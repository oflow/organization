package toolbox

this.metaClass.mixin(cucumber.api.groovy.EN)

Given(~"I have entered (\\d+) into (.*) calculator") { Integer number, String ignore ->
    calc.push number
}

Given(~"(\\d+) into the") {->
    throw new RuntimeException("should never get here since we're running with --guess")
}

When(~"I press (\\w+)") { String opname ->
    result = calc."$opname"()
}

Then(~"the stored result should be (.*)") { Double expected ->
    assert expected == result
}
