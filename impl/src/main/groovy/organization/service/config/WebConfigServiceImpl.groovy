package organization.service.config

@WebConfigService
class WebConfigServiceImpl implements IWebConfigService {
    @RequestDispatcherConfigService
    IRequestDispatcherConfigService requestDispatcherConfigService
    @UiConfigService
    IUiConfigService uiConfigService
    @WebflowConfigService
    IWebflowConfigService webflowConfigService

    def load() {
        loadController()
        loadView()
        loadWebflow()
    }

    def loadController() {}

    def loadView() {}

    def loadWebflow() {}
}
