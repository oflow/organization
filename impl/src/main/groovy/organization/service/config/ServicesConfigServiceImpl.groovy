package organization.service.config

@ServicesConfigService
class ServicesConfigServiceImpl implements IServicesConfigService {
    @ResourcesConfigService
    IResourcesConfigService resourcesConfigService
    @I18nConfigService
    II18nConfigService i18nConfigService
    @CacheConfigService
    ICacheConfigService cacheConfigService
    @AuthorizationConfigService
    IAuthorizationConfigService authorizationConfigService
}
