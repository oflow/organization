package organization.service.config

@ApplicationConfigService
class ApplicationConfigServiceImpl implements IApplicationConfigService {
    @ServicesConfigService
    IServicesConfigService servicesConfigService
    @WebConfigService
    IWebConfigService webConfigService
}
