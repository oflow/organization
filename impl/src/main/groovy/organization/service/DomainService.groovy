package organization.service

import org.springframework.transaction.annotation.Transactional
import organization.repository.Domain

import javax.enterprise.inject.Default
import javax.inject.Named
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaQuery

@Default
@DomainServiceQualifier
@Named('domainService')
class DomainService implements IDomainService {

    @PersistenceContext
    EntityManager em

    private persist = { Domain model -> em.persist model; model }

    @Transactional
    Domain save(Domain model) { model ? model.id ? em.merge(model) : persist(model) : model }

    @Transactional(readOnly = true)
    Domain find(Class entityClass, Serializable id) { em.find entityClass, id }

    @Transactional
    void delete(Domain model) { em.remove save(model) }

    @Transactional(readOnly = true)
    List<? extends Domain> findAll(Class<? extends Domain> entityClass) {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select query.from(entityClass)
        em.createQuery(query).resultList as List<? extends Domain>
    }

    @Transactional(readOnly = true)
    List<? extends Domain> findAll(Class<? extends Domain> entityClass, Integer[] range) {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select query.from(entityClass)
        def q = em.createQuery(query)
        q.maxResults = range[1] - range[0]
        q.firstResult = range[0]
        q.resultList as List<? extends Domain>
    }

    @Transactional(readOnly = true)
    Integer count(Class<? extends Domain> entityClass) {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select em.criteriaBuilder.count(query.from(entityClass))
        (em.createQuery(query).singleResult as Long).intValue()
    }

    @Transactional
    List<? extends Domain> updateAll(Collection<? extends Domain> models) { models.each { save it } }

    @Transactional
    List<? extends Domain> saveAll(Collection<? extends Domain> models) {
        return null
    }

    @Transactional
    void deleteAll(Collection<? extends Domain> models) { models.each { delete it } }
}
