package organization.service.navigation

import org.springframework.stereotype.Service
import javax.enterprise.inject.Default
import javax.inject.Named

@Service
@Default
@Named('navigationService')
@NavigationServiceQualifier
class NavigationService implements INavigationService {
}
