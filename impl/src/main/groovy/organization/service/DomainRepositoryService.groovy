package organization.service

import groovy.transform.ToString

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import org.springframework.transaction.annotation.Transactional

@ToString(includeNames = true)
class DomainRepositoryService {

    @PersistenceContext
    transient EntityManager em

    static final EntityManager getEntityManager() {
        EntityManager em = new DomainRepositoryService().em
        if (!em) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)")
        em
    }

    @Transactional(readOnly = true)
    static long countLangs() {
        entityManager.createQuery("SELECT COUNT(o) FROM Lang o", Long.class).getSingleResult()
    }

    @Transactional
    void persist() {
        if (!em) em = entityManager
        em.persist this
    }

    @Transactional
    void flush() {
        if (!em) em = entityManager
        em.flush()
    }

    @Transactional
    void clear() {
        if (!em) em = entityManager
        em.clear()
    }
}
//    @Transactional(readOnly = true)
//   static List<Lang> findAllLangs() {
//        return em().createQuery("SELECT o FROM Lang o", Lang.class).getResultList()
//    }
//    @Transactional(readOnly = true)
//    static Lang findLang(Long id) {
//        if (id == null) return null
//        return em().find(Lang.class, id)
//    }
//    @Transactional(readOnly = true)
//    static List<Lang> findLangEntries(int firstResult, int maxResults) {
//        return em().createQuery("SELECT o FROM Lang o", Lang.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList()
//    }
//    @Transactional
//    void remove() {
//        if (this.em == null) this.em = em()
//        if (this.em.contains(this)) {
//            this.em.remove(this)
//        } else {
//            Lang attached = Lang.findLang(this.id)
//            this.em.remove(attached)
//        }
//    }
//    @Transactional
//    Lang merge() {
//        if (this.em == null) this.em = em()
//        Lang merged = this.em.merge(this)
//        this.em.flush()
//        return merged
//    }
