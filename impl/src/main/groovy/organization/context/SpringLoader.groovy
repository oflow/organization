package organization.context

import groovy.text.SimpleTemplateEngine
import org.springframework.core.io.ClassPathResource
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext
import organization.BeanHelper
import organization.model.config.ApplicationConfigModel
import organization.model.config.DataSourceConfigModel

import javax.servlet.ServletContext

//todo:    //une methode qui regarde si il existe un application configuration model xml dans une pattern de path donné en argument
//todo:    //une methode qui genere un application configuration model xml vide a la destination donné en argument
//todo:    //une methode qui prend un model et genere son xsd
//                '<%dataSourceConfigModelMap.keySet().each {%>\n' +
//                        '<%print it%>(${dataSourceConfigModelMap["dataSource"].implementation}) {\n' +
//                        '\tdriverClassName = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tdatabase = "\${dataSourceConfigModelMap[\'dataSource\'].database}",\n' +
////                        'database= "\${dataSourceConfigModelMap[\'<%print it%>\'].database}",\n' +
//                        '\tddl = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tdialect = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tpackagesToScan = ["${dataSourceConfigModelMap["dataSource"].driverClassName}"],\n' +
//                        '\turl = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tusername = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tpassword = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
//                        '\tshowSql = "${dataSourceConfigModelMap["dataSource"].driverClassName}"\n' +
//                        '}' +
//                        '<%}%>' +
//                        '\n'

////                "<%dataSourceConfigModelMap.keySet().each {%>\n" +
//                        '$key1' +
////                        '(<% print "${dataSourceConfigModelMap["${key1}"].implementation}"%>) {\n'+
////                        '\tdriverClassName = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tdatabase = "\${dataSourceConfigModelMap[\'dataSource\'].database}",\n' +
//////                        'database= "\${dataSourceConfigModelMap[\'<%print it%>\'].database}",\n' +
////                        '\tddl = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tdialect = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tpackagesToScan = ["${dataSourceConfigModelMap["dataSource"].driverClassName}"],\n' +
////                        '\turl = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tusername = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tpassword = "${dataSourceConfigModelMap["dataSource"].driverClassName}",\n' +
////                        '\tshowSql = "${dataSourceConfigModelMap["dataSource"].driverClassName}"\n' +
////                        "}" +
////                        "<%}%>" +
//                        "\n"


class SpringLoader extends AbstractWebApplicationContextLoaderHelper<AnnotationConfigWebApplicationContext, ApplicationConfigModel> {
    //////////////////////////////////////////////////////////
    //todo:move to trash when dev finished
    AnnotationConfigWebApplicationContext load() {
        printBeans servletContext, super.load() as AnnotationConfigWebApplicationContext
    }

    static AnnotationConfigWebApplicationContext printBeans(ServletContext sc, AnnotationConfigWebApplicationContext ac) {
        println sc?.serverInfo
        println "beans list :"
        ac?.beanDefinitionNames?.each { println it }
        println "\n\nconfigObject content : "
        println ac?.getBean(ConfigObject)?.toMapString()
        println "\n\n"
        ac
    }
    //////////////////////////////////////////////////////////


    

    AnnotationConfigWebApplicationContext loadBeansDefinition() {
        def key = BeanHelper.simpleInstanceName(ApplicationConfigModel)
        def fileName = "${key}.xml"
        applicationContext = new AnnotationConfigWebApplicationContext()
        applicationContext.register BeansConfiguration
        applicationContext.refresh()
        applicationContext.getBean(ConfigObject).merge new ConfigObject([("$key"): ApplicationConfigModel.parseXml(new ClassPathResource(fileName)?.file?.text)])
        applicationConfigModel = (configObject = applicationContext.getBean(ConfigObject))[key] as ApplicationConfigModel
        applicationContext
    }

    AnnotationConfigWebApplicationContext loadConfiguration() {
        applicationContext
    }

    AnnotationConfigWebApplicationContext loadServices() { applicationContext }

    AnnotationConfigWebApplicationContext loadMvc() { applicationContext }

    AnnotationConfigWebApplicationContext loadBasePackages() {
        if (applicationContext.containsBeanDefinition(BeanHelper.simpleInstanceName(ConfigObject))) {
            configObject = applicationContext.getBean(ConfigObject)
            if (configObject && configObject.containsKey(BeanHelper.simpleInstanceName(ApplicationConfigModel))) {
                applicationConfigModel = configObject[BeanHelper.simpleInstanceName(ApplicationConfigModel)] as ApplicationConfigModel
                if (applicationConfigModel && applicationConfigModel.basePackages && !applicationConfigModel.basePackages.empty) {
                    applicationContext.scan applicationConfigModel.basePackages.toArray() as String[]
                    applicationContext.refresh()
                }
            }
        }
        applicationContext
    }

    static Map dataSourceConfigModelMapTask(Map<String, DataSourceConfigModel> dataSourceConfigModelMap) {
        def dslBeanMap = [:]
        dataSourceConfigModelMap.keySet().each {
            String beanName ->
                def model = ["$beanName": dataSourceConfigModelMap[beanName].properties]
                def engine = new SimpleTemplateEngine()
                def text = DS_BEAN_TEMPLATE
//                dataSourceConfigModelMap.keySet().each { String dataSourceConfigModelKey ->
//                    model.put(dataSourceConfigModelKey, dataSourceConfigModel.properties)
//                }
                def template = engine.createTemplate(text).make([beanName: beanName])
                println template.toString()
                dslBeanMap.putAll model
        }
        dslBeanMap
    }
static final DS_BEAN_TEMPLATE =
            '${beanName}(<% print "\\${dataSourceConfigModelMap[\\""%>${beanName}<% print "\\"].implementation}"%>){\n' + "}\n"


    static void main(args) {
        def model = defaultConfigModel()
        def groovyDslBeanMap = [:]
        model.properties.keySet().each {
            switch (it) {
                case "dataSourceConfigModelMap":
                    println "case \"dataSourceConfigModelMap\""
                    groovyDslBeanMap.putAll dataSourceConfigModelMapTask(model.dataSourceConfigModelMap)
                    break
            }
        }
        println "groovyDslBeanMap.toMapString() : ${groovyDslBeanMap.toMapString()}"
    }

    //todo:move to trash when dev finished
    static ApplicationConfigModel defaultConfigModel() {
        def model = new ApplicationConfigModel(
                basePackages: ["organization"],
                dataSourceConfigModelMap: [
                        "dataSource":
                                new DataSourceConfigModel(
                                        implementation: "org.apache.tomcat.jdbc.pool.DataSource",
                                        database: "H2",
                                        ddl: "create",
                                        dialect: "org.hibernate.dialect.H2Dialect",
                                        packagesToScan: ["organization.repository"],
                                        driverClassName: "org.h2.Driver",
                                        url: "jdbc:h2:mem:web;DB_CLOSE_DELAY=-1",
                                        username: "sa",
                                        password: "",
                                        showSql: false)])
        println model.toXml()
        model
    }
}

//    def initWeb(ApplicationContext applicationContext, ServletContext servletContext) {
//        Map contextParams = [
//                'javax.faces.DEFAULT_SUFFIX': ".xhtml",
//                'javax.faces.PROJECT_STAGE': "Development",
//                'javax.faces.FACELETS_REFRESH_PERIOD': "1",
//                'defaultHtmlEscape': "true",
//                'javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL': "true"
//        ]
//        contextParams.each { String key, String value ->
//            servletContext.setInitParameter(key, value)
//        }
//        def facesServletName = BeanHelper.simpleInstanceName(FacesServlet)
//        ServletRegistration.Dynamic facesServletRegistration =
//                servletContext.addServlet(facesServletName, new FacesServlet())
//        facesServletRegistration.addMapping "*.faces"
//        facesServletRegistration.loadOnStartup = 1
//        def dispatcherServletName = BeanHelper.simpleInstanceName(DispatcherServlet)
//        ServletRegistration.Dynamic dispatcherRegistration = servletContext
//                .addServlet(dispatcherServletName, new DispatcherServlet(applicationContext: applicationContext))
//        dispatcherRegistration.addMapping "/ui/*"
//        dispatcherRegistration.loadOnStartup = 2
//    }

////<display-name>compuserv</display-name>
////    <description>compuserv</description>
////<filter>
////<filter-name>charEncodingFilter</filter-name>
////        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
////<init-param>
////<param-name>encoding</param-name>
////            <param-value>UTF-8</param-value>
////</init-param>
////        <init-param>
////            <param-name>forceEncoding</param-name>
////<param-value>true</param-value>
////        </init-param>
////</filter>
////    <filter-mapping>
////        <filter-name>charEncodingFilter</filter-name>//}
////<url-pattern>/*</url-pattern>
////    </filter-mapping>
////    <!--<filter>-->
////    <!--<filter-name>Spring OpenEntityManagerInViewFilter</filter-name>-->
////    <!--<filter-class>org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter</filter-class>-->
////    <!--</filter>-->
////    <!--<filter-mapping>-->
////    <!--<filter-name>Spring OpenEntityManagerInViewFilter</filter-name>-->
////    <!--<url-pattern>/*</url-pattern>-->
////    <!--</filter-mapping>-->
////    <filter>
////        <filter-name>PrimeFaces FileUpload Filter</filter-name>
////        <filter-class>org.primefaces.webapp.filter.FileUploadFilter</filter-class>
////    </filter>
////    <filter-mapping>
////        <filter-name>PrimeFaces FileUpload Filter</filter-name>
////        <servlet-name>Faces Servlet</servlet-name>
////    </filter-mapping>
////    <listener>
////        <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
////    </listener>
////    <listener>
////        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
////    </listener>
////    <session-config>
////        <session-timeout>10</session-timeout>
////    </session-config>
////    <welcome-file-list>
////        <welcome-file>index.html</welcome-file>
////    </welcome-file-list>
////    <!--<error-page>-->
////    <!--<location>/ui/page_not_found.faces</location>-->
////    <!--</error-page>-->
////</web-app>
//

