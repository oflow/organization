package organization.context

import groovy.util.logging.Slf4j
import org.springframework.beans.factory.BeanCreationException
import org.springframework.context.ApplicationContext
import org.springframework.web.context.support.XmlWebApplicationContext

import javax.servlet.ServletContextEvent

@Slf4j
class DataSourceContextResourcesBuilder {
    static final String VALIDATE = "validate"
    static final String UPDATE = "update"
    /**
     * "classpath*:META-INF/spring/pre_validate-applicationContext.xml"
     * @return
     */
    Map validateDbConfigLocations
    /**
     * "classpath*:META-INF/spring/pre_create-applicationContext.xml"
     * @return
     */
    Map createDbConfigLocations

    void contextInitialized() {
        //validate map and construct validateDbConfigLocations and createDbConfigLocations
        log.info 'Database initialization started'
        try {
            ApplicationContext context = new XmlWebApplicationContext()
            context.setConfigLocations([validateDbConfigLocations[VALIDATE]] as String[])
            context.refresh()
            context.close()
            log.info 'Database validated'
        } catch (BeanCreationException bce) {
            log.warn(bce.toString())
            log.info 'Database not validated : trying to create...'
            try {
                ApplicationContext context = new XmlWebApplicationContext()
                context.setConfigLocations([createDbConfigLocations] as String[])
                context.refresh()
                context.close()
                log.info 'Database generated'
            } catch (BeanCreationException _bce) {
                log.info 'Database not generated'
                log.debug _bce.message
            }
        }
        log.info 'Database initialization finished'
    }

    void contextDestroyed(ServletContextEvent sce) {

    }
}
