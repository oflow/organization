package organization.context

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BeansConfiguration {
    @Bean
    ConfigObject configObject() { new ConfigObject() }
}
