package organization.context.template


abstract class AbstractBeanDslTemplate {
    String beanName
    StringBuilder builder

    String getText() {
        builder.toString()
    }
}
