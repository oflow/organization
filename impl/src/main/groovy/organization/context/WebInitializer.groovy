package organization.context

import org.springframework.web.WebApplicationInitializer

import javax.servlet.ServletContext
import javax.servlet.ServletException

class WebInitializer implements WebApplicationInitializer {

    void onStartup(ServletContext sc) throws ServletException {
        new SpringLoader(servletContext: sc).load()
    }
}
