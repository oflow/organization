package organization.validator.groups

import javax.validation.GroupSequence
import javax.validation.groups.Default


@GroupSequence([UserChecks.class, Default.class])
interface UserSequenceChecks {}