
package organization.validator.groups


//in brackets '{}' means the message is in ValidationMessages.properties
//without brackets means the message is i18n.messages.properties
interface UserChecks {
    static final String USERNAME_SIZE_VALIDATION_MSG_KEY = "{site.domain.User.username.Size.message}"
    static final String USERNAME_NOT_NULL_VALIDATION_MSG_KEY = "{site.domain.User.username.NotNull.message}"
    static final String PASSWORD_SIZE_VALIDATION_MSG_KEY = "{site.domain.User.password.Size.message}"
    static final String PASSWORD_NOT_NULL_VALIDATION_MSG_KEY = "{site.domain.User.password.NotNull.message}"
    static final String SITE_USER_USERNAME_UNIQUE_MSG_KEY = "site.domain.User.username.unique.message"
    static final String SITE_USER_CURRENT_EMAIL_VALUE_UNIQUE_MSG_KEY = "site.domain.User.currentEmailValue.unique.message"
    static final String SITE_USER_CURRENT_EMAIL_VALUE_RETYPE_DOES_NOT_MATCH = "site.domain.User.currentEmailValue.doesnotmatch.message"
    static final String SITE_USER_PASSWORD_DOES_NOT_MATCH = "site.domain.User.password.doesnotmatch.message"
    static final String SITE_USER_CURRENT_EMAIL_SECONDARY_EMAIL_VALUE_CANNOT_BE_SAME_MSG_KEY = "site.domain.User.currentEmailValue.secondaryEmailValue.cannot.be.same.message"
    static final String USERNAME_EMAIL_NOT_FOUND_MESSAGE_KEY = "authentication.UsernameNotFoundException.message"
    static final String BAD_CREDENTIALS_MESSAGE_KEY = "authentication.BadCredentialsException.message"
    static final String USER_DISABLED_MESSAGE_KEY = "authentication.DisabledException.message"
    static final String ACCOUNT_EXPIRED_MSG_KEY = "authentication.AccountExpiredException.message"
    static final String USER_LOCKED_MSG_KEY = "authentication.LockedException.message"
    static final String CREDENTIAL_EXPIRED_MSG_KEY = "authentication.CredentialsExpiredException.message"

    //TODO:creer un nouveau message pour le mot de passe pas etre reutilisé
}