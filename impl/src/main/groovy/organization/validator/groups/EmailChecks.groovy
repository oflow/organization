package organization.validator.groups


interface EmailChecks {
    static final EMAIL_SIZE_VALIDATION_MSG_KEY = "{site.domain.Email.value.Size.message}"
    static final EMAIL_NOT_NULL_VALIDATION_MSG_KEY = "{site.domain.Email.value.NotNull.message}"
    static final EMAIL_INVALID_VALIDATION_MSG_KEY = "{site.domain.Email.value.Email.message}"
}
