package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.annotation.*

@Entity
@Table(name = "groups")
@NamedQueries([
@NamedQuery(name = GroupDomain.FIND_BY_GROUP_NAME,
        query = "SELECT g FROM Group g WHERE g.groupName = :groupName")])
@XmlRootElement(name = "group")
@XmlType(name = "Group",
        propOrder = ["id", "groupName", "roles", "users"])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true, excludes = ["users", "roles"])
@EqualsAndHashCode(excludes = ["id", "version"])
class Group implements GroupDomain<Role, User> {

    Long parseId(String value) { Long.valueOf(value) }

    @Id
    @GeneratedValue
    @Column(name = "group_id")
    @XmlElement(name = "id", required = true)
    Long id

    @NotNull
    @Size(min = 1, max = 100)
    @Column(length = 100)
    @Index(name = "idx_groups_name")
    @XmlElement(name = "groupName", required = true)
    String groupName

    @ManyToMany
    @JoinTable(name = "group_role",
            joinColumns = [@JoinColumn(name = "group_id", referencedColumnName = "group_id", nullable = false)],
            inverseJoinColumns = [@JoinColumn(name = "role_id", referencedColumnName = "role_id", nullable = false)])
    Set<Role> roles

    @ManyToMany
    @JoinTable(name = "group_user",
            joinColumns = [@JoinColumn(name = "group_id", referencedColumnName = "group_id", nullable = false)],
            inverseJoinColumns = [@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)])
    Set<User> users

    @Version
    Integer version
}