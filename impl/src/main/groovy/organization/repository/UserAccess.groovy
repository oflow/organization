package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index

import javax.persistence.*
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

import static javax.persistence.TemporalType.*
import static javax.xml.bind.annotation.XmlAccessType.*

@Entity
@Table( uniqueConstraints = [
@UniqueConstraint(columnNames = ["user_id", "http_header_id","access_date"])])
@XmlRootElement(name = 'userAccess')
@XmlType(name = 'UserAccess', propOrder = ['id', 'user', 'role'])
@XmlAccessorType(NONE)
@ToString(includeNames = true)
@EqualsAndHashCode
class UserAccess implements UserAccessDomain<User, HttpHeader> {
    @Id
    @XmlElement(name = 'id', required = true)
    Long id
    @NotNull
    @Column(name = "access_date")
    @Temporal(TIMESTAMP)
    @Index(name = "idx_access_date")
    @XmlElement(name = "date", required = true)
    Date date = new Date()
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'user_id',
            referencedColumnName = 'user_id',
            insertable = false, updatable = false)
    @XmlElement(name = 'user', required = true)
    User user

    @NotNull
    @ManyToOne
    @JoinColumn(name = 'http_header_id',
            referencedColumnName = 'http_header_id')
    @XmlElement(name = 'httpHeader', required = true)
    HttpHeader httpHeader

    Long parseId(String value) { null }
}