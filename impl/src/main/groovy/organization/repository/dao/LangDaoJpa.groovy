package organization.repository.dao

import organization.repository.Lang

import javax.enterprise.inject.Default
import javax.inject.Named

@Default
@LangDao
@Named('langDao')
class LangDaoJpa extends DaoJpa<Lang, Long> implements ILangDao<Lang, Long> {
    LangDaoJpa() { super(Lang) }

    Lang addSupportedLang(String code) {
        Lang lang = findByCode(code)
        if (lang) {
            lang.supported = lang.supported ?: true
            save(lang)
        } else {
            save(new Lang(code: code, supported: true))
        }
    }


    Lang findByCode(String code) {
        executeQuery("from Lang l where l.code = :code",
                [(Lang.CODE_FIELD): code]) as Lang
    }
}