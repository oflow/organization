package organization.repository.dao

import organization.repository.Domain

import javax.persistence.*
import javax.persistence.criteria.CriteriaQuery

class DaoJpa<M extends Domain, ID extends Serializable> implements Dao<M, ID> {

    Class<M> type

    DaoJpa(Class<M> type) { this.type = type }

    @PersistenceContext
    EntityManager em

    private M persist(M model) { em.persist model; model }

    M save(M model) { model ? model.id ? em.merge(model) : persist(model) : model }

    void delete(M model) { em.remove save(model) }

    M find(ID id) { em.find type, id }

    List<M> findAll() {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select query.from(type)
        em.createQuery(query).resultList as List<M>
    }

    List<M> findAll(Integer[] range) {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select query.from(type)
        def q = em.createQuery(query)
        q.maxResults = range[1] - range[0]
        q.firstResult = range[0]
        q.resultList as List<M>
    }

    Integer count() {
        CriteriaQuery query = em.criteriaBuilder.createQuery()
        query.select em.criteriaBuilder.count(query.from(type))
        (em.createQuery(query).singleResult as Long).intValue()
    }

    List<M> updateAll(Collection<M> models) { models.each { save it } }

    List<M> saveAll(Collection<M> models) {
        return null
    }

    void deleteAll(Collection<M> models) { models.each { delete it } }

    private dynamicResult = { result ->
        if (result)
            if (result instanceof Collection)
                if (result.isEmpty()) null
                else result.size() == 1 ? result.first() : result
            else result
        else null
    }

    def executeQuery(String strQuery, Map<String, Object> params) {
        Query query = em.createQuery(strQuery, type)
        params?.each { query.setParameter it.key, it.value }
        dynamicResult query.resultList
    }

    def executeQuery(String strQuery, Map<String, Object> params, Integer[] range) {
        Query query = em.createQuery(strQuery, type)
        params?.each { query.setParameter it.key, it.value }
        query.maxResults = range[1] - range[0]
        query.firstResult = range[0]
        dynamicResult query.resultList
    }

    def executeQuery(String strQuery) {
        def result = em.createQuery(strQuery, type).resultList
        dynamicResult result
    }

    def executeQuery(String strQuery, Integer[] range) {
        Query query = em.createQuery(strQuery, type)
        query.maxResults = range[1] - range[0]
        query.firstResult = range[0]
        dynamicResult query.resultList
    }
}