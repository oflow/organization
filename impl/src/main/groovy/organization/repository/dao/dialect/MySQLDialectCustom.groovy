package organization.repository.dao.dialect

import org.hibernate.dialect.MySQL5InnoDBDialect

import java.sql.Types

class MySQLDialectCustom extends MySQL5InnoDBDialect {

    String getTableTypeString() {
        super.tableTypeString + ' DEFAULT CHARSET=utf8'
    }

    MySQLDialectCustom() {
        registerColumnType Types.BIT, 'boolean'
    }
}