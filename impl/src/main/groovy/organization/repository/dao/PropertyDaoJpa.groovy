package organization.repository.dao

import organization.repository.Property

import javax.enterprise.inject.Default
import javax.inject.Named

@Default
@PropertyDao
@Named("propertyDao")
class PropertyDaoJpa extends DaoJpa<Property, Long> implements IPropertyDao<Property, Long> {

    PropertyDaoJpa() { super(Property) }

    Property findByKey(String key) {
        executeQuery('from Property p where p.key=:key', [(Property.KEY_FIELD): key]) as Property
    }

    Property createOrUpdate(String key, String value) {
        createOrUpdate(key, value, false)
    }

    Property createOrUpdate(String key, String value, Boolean updated) {
        def p = findByKey(key) as Property
        if (p) {
            if (updated && p.value != value) {
                p.value = value
                p = save p
            }
            p
        } else {
            save new Property(key: key, value: value)
        }
    }
}