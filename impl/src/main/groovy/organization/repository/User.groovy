package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.NotBlank
import organization.validator.groups.EmailChecks

import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@Entity
@XmlRootElement(name = 'user')
@XmlType(name = 'User', propOrder = [
'id', 'username', 'password',
'enabled', 'accountExpired',
'accountLocked', 'passwordExpired'])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode(excludes = ['id', 'version', 'userRoles'])
@ToString(includeNames = true, excludes = ['userRoles'])
class User implements UserDomain<Profile, Password, UserRole, UserEmail, UserEnabled, UserAccess> {
    Long parseId(String value) { Long.valueOf(value) }
    @Id
    @GeneratedValue
    @Column(name = 'user_id')
    Long id
    @org.hibernate.validator.constraints.Email(message = EmailChecks.EMAIL_INVALID_VALIDATION_MSG_KEY)
    @NotNull(message = EmailChecks.EMAIL_NOT_NULL_VALIDATION_MSG_KEY)
    @Size(max = 255, message = EmailChecks.EMAIL_SIZE_VALIDATION_MSG_KEY)
    @Column(unique = true)
    @XmlElement(name = "email", required = true)
    String email
    @NotNull
    @NotBlank
    @Max(value = 64L)
    @Column(unique = true)
    String username
    @NotNull
    @NotBlank
    @Column(name = '`password`')
    String password
    Boolean enabled = false
    Boolean accountExpired = false
    Boolean accountLocked = false
    Boolean passwordExpired = false
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'user')
    Set<UserRole> userRoles
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'user')
    Set<UserEmail> userEmails
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'user')
    Set<UserEnabled> userEnableds
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'user')
    Set<UserAccess> userAccesses
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'user')
    Set<Password> passwords
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
    Profile profile

    String toXml() {
        Marshaller m = JAXBContext.newInstance(User).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal this, writer
        String xml = writer.toString()
        writer.flush(); writer.close(); xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static User parseXml(String xml) {
        JAXBContext.newInstance(User)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as User
    }

    static User parseJson(String json) {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .readValue(json, User)
    }
}