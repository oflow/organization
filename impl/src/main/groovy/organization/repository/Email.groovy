package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import organization.validator.groups.EmailChecks

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

import static javax.xml.bind.annotation.XmlAccessType.NONE

@Entity
@XmlRootElement(name = 'email')
@XmlType(name = 'Email', propOrder = ['id', EmailDomain.EMAIL_VALUE_FIELD_NAME])
@XmlAccessorType(NONE)
@ToString(includeNames = true, excludes = ['userEmails'])
@EqualsAndHashCode(excludes = ['id', 'userEmails'])
class Email implements EmailDomain<UserEmail> {

    @Id
    @GeneratedValue
    @Column(name = 'email_id')
    @XmlElement(name = 'id', required = true)
    Long id
    @org.hibernate.validator.constraints.Email(message = EmailChecks.EMAIL_INVALID_VALIDATION_MSG_KEY)
    @NotNull(message = EmailChecks.EMAIL_NOT_NULL_VALIDATION_MSG_KEY)
    @Size(max = 255, message = EmailChecks.EMAIL_SIZE_VALIDATION_MSG_KEY)
    @Column(unique = true, name = 'email_value')
    @XmlElement(name = EmailDomain.EMAIL_VALUE_FIELD_NAME, required = true)
    String emailValue
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'email')
    Set<UserEmail> userEmails

    @PrePersist
    @PreUpdate
    void emailValueToLowerCase() { emailValue = !emailValue ?: emailValue.toLowerCase() }

    Long parseId(String value) { Long.valueOf(value) }
}