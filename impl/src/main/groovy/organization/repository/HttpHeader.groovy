package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.annotations.Index
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Entity
@XmlRootElement(name = 'httpHeader')
@XmlType(name = 'HttpHeader',
        propOrder = ['id', 'accept', 'acceptCharset',
        'acceptEncoding', 'acceptLanguage',
        'cacheControl', 'connection', 'cookie',
        'date', 'header', 'host',
        'ipv4address', 'ipv6address',
        'isp', 'keepAlive', 'protocol',
        'referer', 'uri', 'userAgent'])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ['id', 'version', 'browser'])
class HttpHeader implements HttpHeaderDomain<Browser> {

    @Id
    @GeneratedValue
    @Column(name = 'http_header_id')
    @XmlElement(name = 'id', required = true)
    Long id
    @Version
    Integer version
    @ManyToOne
    @JoinColumn(name = 'browser_id',
            referencedColumnName = 'browser_id')
    @XmlElement(name = 'browser', required = true)
    Browser browser
    @Lob
    @Size(max = 2147483647)
    @XmlElement(name = 'accept', required = true)
    String accept
    @Size(max = 255)
    @Index(name = 'idx_header_accept_charset')
    @XmlElement(name = 'acceptCharset', required = true)
    String acceptCharset
    @Size(max = 255)
    @Index(name = 'idx_header_accept_enc')
    @XmlElement(name = 'acceptEncoding', required = true)
    String acceptEncoding
    @Size(max = 255)
    @Index(name = 'idx_header_accept_lang')
    @XmlElement(name = 'acceptLanguage', required = true)
    String acceptLanguage
    @Size(max = 255)
    @Index(name = 'idx_header_cache_ctrl')
    @XmlElement(name = 'cacheControl', required = true)
    String cacheControl
    @Size(max = 16)
    @Index(name = 'idx_header_conn')
    @XmlElement(name = 'connection', required = true)
    String connection
    @Lob
    @Size(max = 2147483647)
    @XmlElement(name = 'cookie', required = true)
    String cookie
    @NotNull
    @Index(name = 'idx_header_date')
    @Column(name = 'header_date')
    @Temporal(TemporalType.TIMESTAMP)
    @XmlElement(name = 'date', required = true)
    Date date = new Date()
    @NotBlank
    @Lob
    @Size(min = 1, max = 2147483647)
    @XmlElement(name = 'header', required = true)
    String header
    @Size(max = 255)
    @Index(name = 'idx_header_host')
    @XmlElement(name = 'host', required = true)
    String host
    @Size(max = 255)
    @Index(name = 'idx_header_ipv4')
    @XmlElement(name = 'ipv4address', required = true)
    String ipv4address
    @Size(max = 255)
    @Index(name = 'idx_header_ipv6')
    @XmlElement(name = 'ipv6address', required = true)
    String ipv6address
    @Size(max = 255)
    @Index(name = 'idx_header_isp')
    @XmlElement(name = 'isp', required = true)
    String isp
    @Size(max = 255)
    @Index(name = 'idx_header_keep_alive')
    @XmlElement(name = 'keepAlive', required = true)
    String keepAlive
    @Size(max = 255)
    @Index(name = 'idx_header_protocol')
    @XmlElement(name = 'protocol', required = true)
    String protocol
    @Size(max = 255)
    @Index(name = 'idx_header_referer')
    @XmlElement(name = 'referer', required = true)
    String referer
    @Size(max = 255)
    @Index(name = 'idx_header_uri')
    @XmlElement(name = 'uri', required = true)
    String uri
    @Lob
    @Size(max = 2147483647)
    @XmlElement(name = 'userAgent', required = true)
    String userAgent

    Long parseId(String value) { Long.valueOf(value) }

    String toXml() {
        Marshaller m = JAXBContext.newInstance(HttpHeader).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal this, writer
        String xml = writer.toString()
        writer.flush(); writer.close(); xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString this
    }

    static HttpHeader parseXml(String xml) {
        JAXBContext.newInstance(HttpHeader)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as HttpHeader
    }

    static HttpHeader parseJson(String json) {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .readValue(json, HttpHeader)
    }
}