package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.annotations.Index
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*


@Cacheable
@Entity
@XmlRootElement(name = 'lang')
@XmlType(name = 'Lang', propOrder =
[LangDomain.ID_FIELD, LangDomain.CODE_FIELD,LangDomain.SUPPORTED_FIELD])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode(excludes = ["id"])
@ToString(includeNames = true)
class Lang implements LangDomain {
    @Id
    @GeneratedValue
    @Column(name = LangDomain.ID_COLUMN)
    @XmlElement(name = LangDomain.ID_FIELD, required = true)
    Long id
    @NotBlank
    @Size(min = 1, max = 8)
    @Column(unique = true)
    @XmlElement(name = LangDomain.CODE_FIELD, required = true)
    String code
    @NotNull
    @Index(name = 'idx_lang_supported')
    @XmlElement(name = LangDomain.SUPPORTED_FIELD, required = true)
    Boolean supported = false

    Long parseId(String value) { Long.valueOf(value) }

    @PreUpdate
    @PrePersist
    void toLowerCase() { code = !code ?: code.toLowerCase() }

    String toXml() {
        def m = JAXBContext.newInstance(Lang).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        def writer = new StringWriter()
        m.marshal this, writer
        String xml = writer.toString()
        writer.flush(); writer.close(); xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString this
    }

    static Lang parseXml(String xml) {
        JAXBContext.newInstance(Lang)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as Lang
    }

    static Lang parseJson(String json) {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .readValue json, Lang
    }
}