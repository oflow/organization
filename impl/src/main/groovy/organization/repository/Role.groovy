package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Cacheable
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ['userRoles'])
@XmlRootElement(name = 'role')
@XmlType(name = 'Role', propOrder = ['id', 'authority'])
@XmlAccessorType(XmlAccessType.NONE)
class Role implements RoleDomain<UserRole> {
    @Id
    @GeneratedValue
    @Column(name = 'role_id')
    @XmlElement(name = 'id', required = true)
    Long id
    @NotBlank
    @NotNull
    @Column(unique = true)
    @XmlElement(name = RoleDomain.AUTHORITY_FIELD_NAME, required = true)
    String authority
    @OneToMany(cascade = CascadeType.ALL, mappedBy = 'role')
    Set<UserRole> userRoles

    @PreUpdate
    @PrePersist
    void toLowerCase() {
        authority = !authority ?: authority.toUpperCase()
    }

    void toUpperCase() {
        authority.toUpperCase()
    }

    Long parseId(String value) {
        Long.valueOf(value)
    }
}