package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index
import organization.GenderTypeEnum

import javax.persistence.*
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Size
import javax.xml.bind.annotation.*

@Entity
@XmlRootElement(name = 'profile')
@XmlType(name = 'Profile', propOrder = [
'id', 'user', 'lang', 'firstName', 'secondName',
'lastName', 'dateOfBirth', 'dateCreated',
'gender', 'signature'])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode(excludes = ['id', 'version', 'user'])
@ToString(includeNames = true)
class Profile implements ProfileDomain<User, Lang> {

    Long parseId(String value) { Long.valueOf(value) }
    @Id
    @GeneratedValue
    @Column(name = 'profile_id')
    @XmlElement(name = 'id', required = true)
    Long id

    @Version
    Integer version

    @Valid
    @NotNull
    @OneToOne
    @JoinColumn(name = 'user_id',
        referencedColumnName = 'user_id')
    @XmlElement(name = 'user', required = true)
    User user

    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'lang_id',
        referencedColumnName = 'lang_id')
    @XmlElement(name = 'lang', required = true)
    Lang lang

    @Size(max = 255)
    @Index(name = 'idx_profile_signature')
    @XmlElement(name = 'signature', required = true)
    String signature

    @Size(min = 1, max = 255)
    @Index(name = 'idx_profile_last_name')
    @XmlElement(name = 'lastName', required = true)
    String lastName

    @Size(max = 255)
    @Index(name = 'idx_profile_second_name')
    @XmlElement(name = 'secondName', required = true)
    String secondName

    @Size(min = 1, max = 255)
    @Index(name = 'idx_profile_first_name')
    @XmlElement(name = 'firstName', required = true)
    String firstName

    @Index(name = 'idx_profile_gender')
    @Enumerated(EnumType.STRING)
    @XmlElement(name = 'gender', required = true)
    GenderTypeEnum gender = GenderTypeEnum.GENDER_PRIVACY_RESPECT

    @Past
    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = 'idx_profile_dob')
    @XmlElement(name = 'dateOfBirth', required = true)
    Date dateOfBirth

    @NotNull
    @Column(name = 'registered_date')
    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = 'idx_profile_reg_date')
    @XmlElement(name = 'dateCreated', required = true)
    Date dateCreated = new Date()
}