package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Table(name = 'user_role', uniqueConstraints = [
@UniqueConstraint(columnNames = ["user_id", "role_id"])])
@XmlRootElement(name = 'userRole')
@XmlType(name = 'UserRole', propOrder = ['id', 'user', 'role'])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ['id', 'version'])
class UserRole implements UserRoleDomain<User, Role> {
    @Id
    @XmlElement(name = 'id', required = true)
    Long id
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'user_id',
            referencedColumnName = 'user_id',
            insertable = false, updatable = false)
    @XmlElement(name = 'user', required = true)
    User user
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'role_id',
            referencedColumnName = 'role_id',
            insertable = false, updatable = false)
    @XmlElement(name = 'role', required = true)
    Role role
    @Version
    Integer version

    Long parseId(String value) {
        Long.valueOf(value)
    }
}