package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*


@Entity
@Cacheable
@XmlRootElement(name = 'property')
@XmlType(name = 'Property', propOrder = [
'id', PropertyDomain.KEY_FIELD, PropertyDomain.VALUE_FIELD])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true, excludes = ['id', 'version'])
@EqualsAndHashCode(excludes = ['id', 'version'])
class Property implements PropertyDomain {

    @Id
    @GeneratedValue
    @Column(name = PropertyDomain.ID_COLUMN)
    @XmlElement(name = 'id', required = true)
    Long id
    @NotBlank
    @Size(min = 1, max = 255)
    @Column(name = PropertyDomain.KEY_COLUMN, unique = true)
    @XmlElement(name = PropertyDomain.KEY_FIELD, required = true)
    String key
    @NotBlank
    @Size(min = 1, max = 4096)
    @Column(name = PropertyDomain.VALUE_COLUMN, length = 4096)
    @XmlElement(name = PropertyDomain.VALUE_FIELD, required = true)
    String value
    @Version
    Integer version

    @PreUpdate
    @PrePersist
    void toLowerCase() { key = !key ?: key.toLowerCase() }

    Long parseId(String value) { Long.valueOf(value) }

    String toXml() {
        Marshaller m = JAXBContext.newInstance(Property).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal this, writer
        String xml = writer.toString()
        writer.flush(); writer.close(); xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector: new JaxbAnnotationIntrospector()).writeValueAsString(this)
    }

    static Property parseXml(String xml) {
        JAXBContext.newInstance(Property).createUnmarshaller().unmarshal(new StringReader(xml)) as Property
    }

    static Property parseJson(String json) {
        new ObjectMapper(annotationIntrospector: new JaxbAnnotationIntrospector()).readValue(json, Property)
    }
}