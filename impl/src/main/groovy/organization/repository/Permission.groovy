package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.annotation.*

@Cacheable
@Entity
@XmlRootElement(name = "permission")
@XmlType(name = "Permission",
        propOrder = [
        "id",
        "permissionValue"
        ])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ["id"])
class Permission implements PermissionDomain {
    Long parseId(String value) { Long.valueOf(value) }
    @Id
    @GeneratedValue
    @Column(name = "permission_id")
    @XmlElement(name = "id", required = true)
    Long id

    @NotNull
    @Size(min = 1, max = 255)
    @Column(unique = true)
    @XmlElement(name = PermissionDomain.PERMISSION_VALUE_FIELD_NAME, required = true)
    String permissionValue
}
