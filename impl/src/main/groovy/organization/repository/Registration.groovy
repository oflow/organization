package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@Entity
@Table(name = 'registration_code')
@XmlRootElement(name = 'registration')
@XmlType(name = 'Registration',
        propOrder = ['id', 'dateCreated', 'token', 'username'])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode
class Registration implements RegistrationDomain {
    @Id
    @GeneratedValue
    @Column(name = 'registration_code_id')
    Long id
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    Date dateCreated
    @NotBlank
    @Size(min = 1, max = 255)
    String token
    @NotBlank
    @Size(min = 1, max = 255)
    String username

    Long parseId(String value) {
        Long.valueOf(value)
    }
}