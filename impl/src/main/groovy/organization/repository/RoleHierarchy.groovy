package organization.repository


import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Cacheable
@Entity
@NamedQueries([
@NamedQuery(name = RoleHierarchyDomain.FIND_BY_RANK,
        query = "select rh from RoleHierarchy rh where rh.rank = :rank"),
@NamedQuery(name = RoleHierarchyDomain.FIND_BY_ROLE,
        query = "select rh from RoleHierarchy rh where rh.role = :role")])
@XmlRootElement(name = "roleHierarchy")
@XmlType(name = "RoleHierarchy",
        propOrder = [
        "id",
        "rank",
        "role"])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ["id"])
class RoleHierarchy implements RoleHierarchyDomain<Role> {
    Long parseId(String value) { Long.valueOf(value) }
    @Id
    @GeneratedValue
    @Column(name = "role_hierarchy_id")
    @XmlElement(name = "id", required = true)
    Long id

    @Valid
    @NotNull
    @OneToOne
    @JoinColumn(name = "role_id",
            referencedColumnName = "role_id")
    @XmlElement(name = "role", required = true)
    Role role

    @NotNull
    @Min(value = 1L)
    @Column(unique = true)
    @XmlElement(name = "rank", required = true)
    Integer rank
}
