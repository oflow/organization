package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Table(name = "user_enabled",
        uniqueConstraints = [@UniqueConstraint(columnNames = ["user_id", "user_granter_id", "enabled", "start_date"])])
@XmlRootElement(name = "userEnabled")
@XmlType(name = "UserEnabled", propOrder = ["id", "user", "endDate"])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ["version"])
class UserEnabled implements UserEnabledDomain<User> {
    @Id
    @XmlElement(name = "id", required = true)
    Long id

    @NotNull
    @Index(name = "idx_user_enabled_enabled")
    @XmlElement(name = "enabled", required = true)
    Boolean enabled

    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "idx_user_enabled_start_date")
    @XmlElement(name = "startDate", required = true)
    Date startDate = new Date()

    @Version
    Integer version

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "idx_user_enabled_end_date")
    @XmlElement(name = "endDate", required = true)
    Date endDate

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id",
            referencedColumnName = "user_id",
            insertable = false, updatable = false)
    @XmlElement(name = "user", required = true)
    User user

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_granter_id",
            referencedColumnName = "user_id",
            insertable = false, updatable = false)
    @XmlElement(name = "userGranter", required = true)
    User userGranter

    @ManyToOne
    @JoinColumn(name = "user_revoker_id",
            referencedColumnName = "user_id")
    @XmlElement(name = "userRevoker", required = true)
    User userRevoker

    Long parseId(String value) { Long.valueOf(value) }
}