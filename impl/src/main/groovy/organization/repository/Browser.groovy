package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.annotations.Index
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Entity
@XmlRootElement(name = "browser")
@XmlType(name = "Browser", propOrder = [
"id", 
BrowserDomain.NAME_FIELD,
BrowserDomain.VERSION_FIELD,
BrowserDomain.OS_FIELD])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true, excludes = ["headers"])
@EqualsAndHashCode(excludes = ["id", "headers"])
class Browser implements BrowserDomain<HttpHeader> {
    @Id
    @GeneratedValue
    @Column(name = BrowserDomain.ID_COLUMN)
    @XmlElement(name = "id", required = true)
    Long id
    @NotBlank
    @Size(min = 1, max = 255)
    @Index(name = "idx_browser_version")
    @XmlElement(name = BrowserDomain.VERSION_FIELD, required = true)
    String browserVersion
    @NotBlank
    @Size(min = 1, max = 255)
    @Index(name = "idx_browser_name")
    @XmlElement(name = BrowserDomain.NAME_FIELD, required = true)
    String browserName
    @NotBlank
    @Size(min = 1, max = 255)
    @Index(name = "idx_browser_os_name")
    @XmlElement(name = BrowserDomain.OS_FIELD, required = true)
    String browserOs
    @OneToMany(mappedBy = "browser")
    Set<HttpHeader> headers

    String toXml() {
        def m = JAXBContext.newInstance(Browser).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        def writer = new StringWriter()
        m.marshal this, writer
        def _xml = writer.toString()
        writer.flush()
        writer.close()
        _xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static Browser parseXml(String xml) {
        JAXBContext.newInstance(Browser)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as Browser
    }

    static Browser parseJson(String json) {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .readValue(json, Browser)
    }


    Long parseId(String value) { Long.valueOf(value) }
}