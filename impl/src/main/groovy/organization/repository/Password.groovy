package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index

import javax.persistence.*
import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Table(name = 'passwords')
@XmlRootElement(name = 'password')
@XmlType(name = 'Password',
    propOrder = [
        'id',
        'value',
        'startDate',
        'endDate',
        'user'])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ['id', 'version'])
class Password implements PasswordDomain<User>{
    @Id
    @GeneratedValue
    @Column(name = 'password_id')
    Long id
    @NotNull
    @Index(name = 'idx_password_value')
    @Column(name = 'password_value')
    String passwordValue
    @NotNull
    @Index(name = 'idx_password_start_date')
    Date startDate = new Date()
    @Index(name = 'idx_password_end_date')
    Date endDate
    @Valid
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'user_id',
        referencedColumnName = 'user_id'
        //    , insertable = false
        //    , updatable = false
    )
    @XmlElement(name = 'user', required = true)
    User user
    @Version
    Integer version

    Long parseId(String value) {
        Long.valueOf(value)
    }
}