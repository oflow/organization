package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.validator.constraints.NotBlank

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Cacheable
@ToString
@EqualsAndHashCode
@XmlRootElement(name = 'requestMap')
@XmlType(name = 'RequestMap',
        propOrder = ['id', 'url', 'configAttribute'])
@XmlAccessorType(XmlAccessType.NONE)
class RequestMap implements RequestMapDomain {
    Long parseId(String value) { Long.valueOf(value) }
    @Id
    @GeneratedValue
    @Column(name = 'request_map_id')
    @XmlElement(name = 'id', required = true)
    Long id
    @NotNull
    @NotBlank
    @Column(unique = true)
    String url
    @NotBlank
    String configAttribute
}