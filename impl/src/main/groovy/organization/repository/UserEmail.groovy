package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.annotations.Index

import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.xml.bind.annotation.*

@Entity
@Table(name = 'user_email',uniqueConstraints = [
        @UniqueConstraint(columnNames = ["user_id","email_id","primary_email","start_date"])
])
@XmlRootElement(name = 'userEmail')
@XmlType(name = 'UserEmail',
        propOrder = ['id', 'user', 'email', 'endDate'])
@XmlAccessorType(XmlAccessType.NONE)
@ToString(includeNames = true, excludes = ['version'])
@EqualsAndHashCode(excludes = ['version', 'pending'])
class UserEmail implements UserEmailDomain<User, Email> {
    static final String USER_FIELD_NAME = 'user'
    @Id
    @XmlElement(name = 'id', required = true)
    Long id
    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = 'idx_user_email_end_date')
    @XmlElement(name = 'endDate', required = true)
    Date endDate
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'user_id',
            referencedColumnName = 'user_id',
            insertable = false, updatable = false)
    @XmlElement(name = 'user', required = true)
    User user
    @NotNull
    @ManyToOne
    @JoinColumn(name = 'email_id',
            referencedColumnName = 'email_id',
            insertable = false, updatable = false)
    @XmlElement(name = 'email', required = true)
    Email email
    @Version
    Integer version
    @NotNull
    @Column(name = 'primary_email')
    @Index(name = 'idx_user_email_is_primary')
    @XmlElement(name = 'primaryEmail', required = true)
    Boolean primaryEmail
    @NotNull
    @Column(name = 'start_date')
    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = 'idx_user_email_start_date')
    @XmlElement(name = 'startDate', required = true)
    Date startDate = new Date()

    Long parseId(String value) { Long.valueOf(value) }
}