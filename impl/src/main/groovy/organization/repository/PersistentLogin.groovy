package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.*
import javax.validation.constraints.Max

@Entity
@Table(name = 'persistent_logins')
@ToString
@EqualsAndHashCode
class PersistentLogin implements PersistentLoginDomain {

    String parseId(String value) { value }
    @Id
    @Column(name = 'series')
    @Max(value = 64L)
    String id
    @Max(value = 64L)
    String username
    @Max(value = 64L)
    String token
    Date lastUsed
    @Transient
    transient String series

    void setSeries(String series) { id = series }

    String getSeries() { id }
}
