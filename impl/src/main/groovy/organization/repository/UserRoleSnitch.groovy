package organization.repository

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Version
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@Entity
@XmlRootElement(name = 'userRoleSnitch')
@XmlType(name = 'UserRoleSnitch', propOrder = ['id'])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode(excludes = ['id', 'version'])
@ToString(includeNames = true)
class UserRoleSnitch
//extends UserRole implements UserRoleSnitchDomain
{
    @Id
    @XmlElement(name = 'id', required = true)
    Long id
    @Version
    Integer version

    Long parseId(String value) { Long.valueOf(value) }
}