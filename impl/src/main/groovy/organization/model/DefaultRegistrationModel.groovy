package organization.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.hibernate.validator.constraints.Email

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

import static organization.validator.groups.EmailChecks.*
import static organization.validator.groups.UserChecks.*

@ToString(includeNames = true, includeSuper = true)
@EqualsAndHashCode
class DefaultRegistrationModel extends DefaultUserModel implements RegistrationModel {
    @NotNull(message = USERNAME_NOT_NULL_VALIDATION_MSG_KEY)
    @Size(min = 2, max = 64, message = USERNAME_SIZE_VALIDATION_MSG_KEY)
    String username
    @NotNull(message = EMAIL_NOT_NULL_VALIDATION_MSG_KEY)
    @Email(message = EMAIL_INVALID_VALIDATION_MSG_KEY)
    @Size(max = 255, message = EMAIL_SIZE_VALIDATION_MSG_KEY)
    String email
    String emailRetype
    String passwordRetype
    String header
}
