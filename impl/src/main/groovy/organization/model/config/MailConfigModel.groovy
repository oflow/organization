package organization.model.config

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.Email

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.constraints.NotNull
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Slf4j
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "mailConfigModel")
@XmlType(name = "MailConfigModel", propOrder = [
"name", "mailSmtpHost", "mailSmtpPort", "mailSmtpUser",
"mailSmtpPassword", "mailSmtpFrom",
"mailSmtpTlsStarttlsPort", "mailSmtpStarttlsEnable"])
@XmlAccessorType(XmlAccessType.NONE)
class MailConfigModel implements Serializable, IMailConfigModel {
    @NotNull
    @XmlElement(name = "name", required = true)
    String name
    @NotNull
    @XmlElement(name = "mailSmtpHost", required = true)
    String mailSmtpHost
    @NotNull
    @XmlElement(name = "mailSmtpPort", required = true)
    String mailSmtpPort
    @NotNull
    @XmlElement(name = "mailSmtpUser", required = true)
    String mailSmtpUser
    @NotNull
    @XmlElement(name = "mailSmtpPassword", required = true)
    String mailSmtpPassword
    @Email
    @XmlElement(name = "mailSmtpFrom", required = true)
    String mailSmtpFrom
    @NotNull
    @XmlElement(name = "mailSmtpTlsStarttlsPort", required = true)
    String mailSmtpTlsStarttlsPort
    @NotNull
    @XmlElement(name = "mailSmtpStarttlsEnable", required = true)
    Boolean mailSmtpStarttlsEnable


    Set<ConstraintViolation<MailConfigModel>> validate() {
        return Validation.buildDefaultValidatorFactory()
                .validator
                .validate(this)
    }

    Boolean isValid() { return validate().empty }

    static String simpleInstanceName() {
        StringBuilder sb = new StringBuilder(MailConfigModel.class.simpleName)
        sb.replace 0, 1, new String(sb.toString().charAt(0)).toLowerCase()
        return sb.toString()
    }

    String toXml() {
        Marshaller m = JAXBContext.newInstance(MailConfigModel.class).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal this, writer
        String configurationModelXml = writer.toString()
        writer.flush()
        writer.close()
        return configurationModelXml
    }

    String toJson() {
        return new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static MailConfigModel parseXml(String xml) {
        return JAXBContext.newInstance(MailConfigModel.class)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as MailConfigModel
    }

    static MailConfigModel parseJson(String json) {
        ObjectMapper mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        return mapper.readValue(json, MailConfigModel.class)
    }
}
