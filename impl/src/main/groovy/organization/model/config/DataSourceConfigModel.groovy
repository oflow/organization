package organization.model.config

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.constraints.NotNull
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Slf4j
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "dataSourceConfigModel")
@XmlType(name = "DataSourceConfigModel", propOrder = [
"implementation",
"driverClassName", "url", "username",
"password", "database", "showSql",
"dialect", "ddl", "packagesToScan"])
@XmlAccessorType(XmlAccessType.NONE)
class DataSourceConfigModel implements Serializable, IDatabaseConfigModel {

    @NotNull
    @XmlElement(name = "driverClassName", required = true)
    String driverClassName
    @NotNull
    @XmlElement(name = "url", required = true)
    String url
    @NotNull
    @XmlElement(name = "username", required = true)
    String username
    @NotNull
    @XmlElement(name = "password", required = true)
    String password
    @NotNull
    @XmlElement(name = "database", required = true)
    String database
    @XmlElement(name = "showSql", required = true)
    boolean showSql
    @NotNull
    @XmlElement(name = "dialect", required = true)
    String dialect
    @NotNull
    @XmlElement(name = "ddl", required = true)
    String ddl
    @XmlElement(name = "packagesToScan", required = true)
    Set<String> packagesToScan
    @XmlElement(name = "implementation", required = true)
    String implementation


    Set<ConstraintViolation<DataSourceConfigModel>> validate() {
        Validation.buildDefaultValidatorFactory()
                .validator
                .validate(this)
    }

    Boolean isValid() { return validate().empty }

    String toXml() {
        def m = JAXBContext.newInstance(DataSourceConfigModel)
                .createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        def writer = new StringWriter()
        m.marshal this, writer
        def xml = writer.toString()
        writer.flush()
        writer.close()
        xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static DataSourceConfigModel parseXml(String xml) {
        JAXBContext.newInstance(DataSourceConfigModel)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as DataSourceConfigModel
    }

    static DataSourceConfigModel parseJson(String json) {
        def mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        mapper.readValue(json, DataSourceConfigModel)
    }
}
