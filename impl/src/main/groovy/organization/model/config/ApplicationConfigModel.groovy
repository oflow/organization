package organization.model.config

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Slf4j
@XmlRootElement(name = "applicationConfigModel")
@XmlType(name = "ApplicationConfigModel",
        propOrder = ["basePackages", "dataSourceConfigModelMap"/*, "siteConfigModel", "mailConfigModels", "contentConfigModel", "securityConfigModel"*/])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode
@ToString(includeNames = true)
class ApplicationConfigModel implements IApplicationConfigModel, Serializable {
    @XmlElement(name = "basePackages", required = true)
    Set<String> basePackages
    @XmlElement(name = "dataSourceConfigModelMap", required = true)
    Map<String, DataSourceConfigModel> dataSourceConfigModelMap
//    @Valid
//    @NotNull
//    @XmlElement(name = "siteConfigModel", required = true)
//    SiteConfigModel siteConfigModel
//    @Valid
//    @NotEmpty
//    @XmlElement(name = "mailConfigModels", required = true)
//    Set<MailConfigModel> mailConfigModels
//
//    @XmlElement(name = "contentConfigModel", required = true)
//    ContentConfigModel contentConfigModel
//
//    @XmlElement(name = "securityConfigModel", required = true)
//    SecurityConfigModel securityConfigModel

    Boolean isValid() { return validate().empty }

    Set<ConstraintViolation<ApplicationConfigModel>> validate() {
        Validation.buildDefaultValidatorFactory().validator.validate this
    }


    String toXml() {
        def m = JAXBContext.newInstance(ApplicationConfigModel).createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        def writer = new StringWriter()
        m.marshal this, writer
        def configurationModelXml = writer.toString()
        writer.flush()
        writer.close()
        configurationModelXml
    }

    String toJson() {
        return new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static ApplicationConfigModel parseXml(String xml) {
        JAXBContext.newInstance(ApplicationConfigModel)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as ApplicationConfigModel
    }

    static ApplicationConfigModel parseJson(String json) {
        def mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        mapper.readValue json, ApplicationConfigModel
    }
}

