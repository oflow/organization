package organization.model.config

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@Slf4j
@XmlRootElement(name = "contentConfigModel")
@XmlType(name = "ContentConfigModel", propOrder = [])
@XmlAccessorType(XmlAccessType.NONE)
@EqualsAndHashCode
@ToString
class ContentConfigModel implements Serializable,IContentConfigModel{


}
