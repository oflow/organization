package organization.model.config

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.Email

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.constraints.NotNull
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.bind.annotation.*

@Slf4j
@ToString
@EqualsAndHashCode
@XmlRootElement(name = "siteConfigModel")
@XmlType(name = "SiteConfigModel",
        propOrder = [
        "name", "siteUrl", "siteName", "siteAdminUsername",
        "siteAdminPassword", "siteAdminEmailValue"])
@XmlAccessorType(XmlAccessType.NONE)
class SiteConfigModel implements Serializable, ISiteConfigModel {

    @NotNull
    @XmlElement(name = "name", required = true)
    String name
    @NotNull
    @XmlElement(name = "siteUrl", required = true)
    String siteUrl
    @NotNull
    @XmlElement(name = "siteName", required = true)
    String siteName
    @NotNull
    @XmlElement(name = "siteAdminUsername", required = true)
    String siteAdminUsername
    @NotNull
    @XmlElement(name = "siteAdminPassword", required = true)
    String siteAdminPassword
    @Email
    @XmlElement(name = "siteAdminEmailValue", required = true)
    String siteAdminEmailValue


    Set<ConstraintViolation<SiteConfigModel>> validate() {
        Validation.buildDefaultValidatorFactory()
                .validator.validate this
    }

    Boolean isValid() { return validate().empty }

    static String simpleInstanceName() {
        StringBuilder sb = new StringBuilder(SiteConfigModel.simpleName)
        sb.replace 0, 1, new String(sb.toString().charAt(0)).toLowerCase()
        return sb.toString()
    }

    String toXml() {
        Marshaller m = JAXBContext.newInstance(SiteConfigModel)
                .createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal this, writer
        String xml = writer.toString()
        writer.flush()
        writer.close()
        xml
    }

    String toJson() {
        new ObjectMapper(annotationIntrospector:
                new JaxbAnnotationIntrospector())
                .writeValueAsString(this)
    }

    static SiteConfigModel parseXml(String xml) {
        JAXBContext.newInstance(SiteConfigModel)
                .createUnmarshaller()
                .unmarshal(new StringReader(xml)) as SiteConfigModel
    }

    static SiteConfigModel parseJson(String json) {
        def mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        mapper.readValue json, SiteConfigModel
    }
}
