package organization.model.config

import groovy.util.logging.Slf4j
import organization.BeanHelper

import javax.xml.bind.JAXBContext
import javax.xml.bind.SchemaOutputResolver
import javax.xml.transform.Result
import javax.xml.transform.stream.StreamResult

@Slf4j
class ConfigurationModelOutputResolver extends SchemaOutputResolver {
    static createConfigurationModel() {
        new ConfigObject([
                (BeanHelper.simpleInstanceName(ApplicationConfigModel)):
                        new ApplicationConfigModel(
//                                basePackages: ["organization"],
                                siteConfigModel: new SiteConfigModel(),
                                dataSourceConfigModelMap: [new DataSourceConfigModel()],
                                mailConfigModels: [new MailConfigModel()],
                                contentConfigModel: new ContentConfigModel(),
                                securityConfigModel: new SecurityConfigModel())])
    }
    String relativeFilename

    Result createOutput(String namespaceURI, String suggestedFileName) throws IOException {
        def file = new File(this.relativeFilename)
        !file.exists() ?: file.delete()
        file.createNewFile()
        def result = new StreamResult(file)
        result.systemId = file.toURI().toURL().toString()
        result
    }
    //todo : a process to save xsd in db
    // the different xsds involved into an organization-application
    //todo : a gradle task just before tomcatRunWar
    //who checks if  confModel.xml exists
    // if not exit tomcatRunWar with a gradle output message
    static void main(String[] args) {
        //creation of the xsd and copy into resources folders
        //and copy of the jar in the griffon app
        def context = JAXBContext.newInstance(ApplicationConfigModel)

        //creation of the xsd file
        def fileName = "${BeanHelper.simpleInstanceName(ApplicationConfigModel)}.xsd"
        def relativeFilename = "src/main/resources/$fileName"
        context.generateSchema new ConfigurationModelOutputResolver(relativeFilename: relativeFilename)
    }

}