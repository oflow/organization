package organization.model
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import organization.repository.Email
import organization.repository.User

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

import static organization.validator.groups.UserChecks.PASSWORD_NOT_NULL_VALIDATION_MSG_KEY
import static organization.validator.groups.UserChecks.PASSWORD_SIZE_VALIDATION_MSG_KEY

@ToString(includeNames = true)
@EqualsAndHashCode
class DefaultUserModel implements UserModel<User, Email>{
    User user
    List<Email> emails
    String secondaryEmailValue
    @NotNull(message = PASSWORD_NOT_NULL_VALIDATION_MSG_KEY)
    @Size(min = 6, max = 32, message = PASSWORD_SIZE_VALIDATION_MSG_KEY)
    String password

    void setSecondaryEmail(Email email) {
        secondaryEmailValue = email ? email.emailValue: null
        emails.set 1, email ? email : new Email()
    }

    void fixEmptyString() {
        if (user.profile.signature == "")
            user.profile.signature = null
        if (user.profile.lastName == "")
            user.profile.lastName = null
        if (user.profile.secondName == "")
            user.profile.secondName = null
        if (user.profile.firstName == "")
            user.profile.firstName = null
        if (secondaryEmailValue == "")
            secondaryEmailValue = null
        if (password == "")
            password = null
    }
}