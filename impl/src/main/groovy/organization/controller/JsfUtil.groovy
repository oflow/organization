package organization.controller

import org.springframework.beans.annotation.AnnotationBeanUtils
import org.springframework.context.ApplicationContext
import org.springframework.core.annotation.AnnotationUtils
import org.springframework.web.jsf.FacesContextUtils

import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.convert.Converter
import javax.faces.model.SelectItem

class JsfUtil {
    AnnotationUtils annotationUtils
    AnnotationBeanUtils annotationBeanUtils

    static ApplicationContext getApplicationContext() {
        FacesContextUtils.getWebApplicationContext FacesContext.currentInstance
    }

    static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
        def size = selectOne ? entities.size() + 1 : entities.size()
        def items = new SelectItem[size]
        def i = 0
        if (selectOne) {
            items[0] = new SelectItem("", "---")
            i++
        }
        entities.each { Object x ->
            items[i++] = new SelectItem(x, x.toString())
        }
        items
    }

    static void addErrorMessage(Exception ex, String defaultMsg) {
        def msg = ex.getLocalizedMessage()
        if (msg != null && msg.length() > 0) addErrorMessage msg
        else addErrorMessage defaultMsg
    }

    static void addErrorMessages(List<String> messages) {
        messages.each { String message -> addErrorMessage message }
    }

    static void addErrorMessage(String msg) {
        def facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg)
        FacesContext.currentInstance.addMessage null, facesMsg
    }

    static void addSuccessMessage(String msg) {
        def facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg)
        FacesContext.currentInstance.addMessage "successInfo", facesMsg
    }

    static String getRequestParameter(String key) {
        FacesContext.currentInstance.externalContext.requestParameterMap[key]
    }

    static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        def theId = getRequestParameter(requestParameterName)
        converter.getAsObject(FacesContext.currentInstance, component, theId)
    }
}


