package organization.controller
import javax.annotation.PostConstruct
import javax.faces.bean.ManagedBean
import javax.faces.bean.SessionScoped
import javax.faces.context.FacesContext
import javax.faces.model.SelectItem

@ManagedBean
@SessionScoped
 class LocaleBean {
    Locale locale

    @PostConstruct
     void init() {
        locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale()
    }

     SelectItem[] getLocales() {
        List<SelectItem> items = new ArrayList<SelectItem>()
        Iterator<Locale> supportedLocales = FacesContext.getCurrentInstance().getApplication().getSupportedLocales()
        while (supportedLocales.hasNext()) {
            Locale locale = supportedLocales.next()
            items.add new SelectItem(locale.toString(), locale.getDisplayName())
        }
         items.toArray([] as SelectItem[])
    }

     String getSelectedLocale() {
         getLocale().toString()
    }

     void setSelectedLocale() {
        setSelectedLocale(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("locale"))
    }

     void setSelectedLocale(String localeString) {
        Iterator<Locale> supportedLocales = FacesContext.getCurrentInstance().getApplication().getSupportedLocales()
        while (supportedLocales.hasNext()) {
            Locale locale = supportedLocales.next()
            if (locale.toString().equals(localeString)) {
                this.locale = locale
                break
            }
        }
    }
}