package organization.controller

import organization.repository.Domain
import organization.service.DomainServiceQualifier
import organization.service.IDomainService
import org.springframework.stereotype.Controller

import javax.enterprise.context.SessionScoped
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.convert.Converter
import javax.faces.model.DataModel
import javax.faces.model.ListDataModel
import javax.faces.model.SelectItem
import javax.inject.Inject

@SessionScoped
abstract class AbstractDomainController<M extends Domain, ID extends Serializable> implements Serializable {

    Class<M> entityClass
    M current
    DataModel<M> items = null
    @Inject
    @DomainServiceQualifier
    IDomainService domainService

    AbstractDomainController(Class<M> clazz) { entityClass = clazz }

    PaginationHelper pagination
    Integer selectedItemIndex

    M getSelected() {
        if (current == null) {
            current = M.newInstance()
            selectedItemIndex = -1
        }
        current
    }

    PaginationHelper getPagination() {
        if (!pagination) {
            pagination = new PaginationHelper(10) {
                int getItemsCount() { domainService.count entityClass }

                DataModel createPageDataModel() {
                    new ListDataModel<? extends Domain>(domainService.findAll(entityClass, [pageFirstItem, pageFirstItem + pageSize] as Integer[]))
                }
            }
        }
        pagination
    }

    String prepareList() {
        recreateModel()
        "list"
    }

    String prepareView() {
        current = getItems().rowData
        selectedItemIndex = pagination.pageFirstItem + getItems().rowIndex
        "view"
    }

    String prepareCreate() {
        current = entityClass.newInstance()
        selectedItemIndex = -1
        "create"
    }

    String create() {
        try {
            domainService.save current
            JsfUtil.addSuccessMessage ResourceBundle.getBundle("/Bundle").getString("PropertyCreated")
            prepareCreate()
        } catch (Exception e) {
            JsfUtil.addErrorMessage e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured")
            null
        }
    }

    String prepareEdit() {
        current = getItems().rowData
        selectedItemIndex = pagination.pageFirstItem + getItems().rowIndex
        "edit"
    }

    String update() {
        try {
            domainService.save current
            JsfUtil.addSuccessMessage ResourceBundle.getBundle("/Bundle").getString("PropertyUpdated")
            "view"
        } catch (Exception e) {
            JsfUtil.addErrorMessage e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured")
            null
        }
    }

    String destroy() {
        current = getItems().rowData
        selectedItemIndex = pagination.pageFirstItem + getItems().rowIndex
        performDestroy()
        recreatePagination()
        recreateModel()
        "list"
    }

    String destroyAndView() {
        performDestroy()
        recreateModel()
        updateCurrentItem()
        if (selectedItemIndex >= 0) "view"
        else {
            // all items were removed - go back to list
            recreateModel()
            "list"
        }
    }

    private void performDestroy() {
        try {
            domainService.delete current
            JsfUtil.addSuccessMessage ResourceBundle.getBundle("/Bundle").getString("PropertyDeleted")
        } catch (Exception e) {
            JsfUtil.addErrorMessage e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured")
        }
    }

    private void updateCurrentItem() {
        Integer count = domainService.count(entityClass)
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1
            // go to previous page if last page disappeared:
            if (pagination.pageFirstItem >= count) pagination.previousPage()
        }
        if (selectedItemIndex >= 0) current = domainService.findAll(entityClass, [selectedItemIndex, selectedItemIndex + 1] as Integer[]).first()
    }

    DataModel<M> getItems() {
        if (items == null) items = getPagination().createPageDataModel()
        items
    }

    private void recreateModel() {
        items = null
    }

    private void recreatePagination() {
        pagination = null
    }

    String next() {
        getPagination().nextPage()
        recreateModel()
        "list"
    }

    String previous() {
        getPagination().previousPage()
        recreateModel()
        "list"
    }

    SelectItem[] getItemsAvailableSelectMany() {
        JsfUtil.getSelectItems domainService.findAll(entityClass), false
    }

    SelectItem[] getItemsAvailableSelectOne() {
        JsfUtil.getSelectItems domainService.findAll(entityClass), true
    }

    M getModel(ID id) { domainService.find entityClass, id }
}

abstract class PaginationHelper {
    abstract int getItemsCount()

    abstract DataModel createPageDataModel()

    Integer pageSize
    Integer page

    PaginationHelper(Integer pageSize) { this.pageSize = pageSize }

    Integer getPageFirstItem() { page * pageSize }

    Integer getPageLastItem() {
        Integer i = getPageFirstItem() + pageSize - 1
        Integer count = getItemsCount() - 1
        if (i > count) i = count
        if (i < 0) i = 0
        if (i < 0) i = 0
        i
    }

    Boolean isHasNextPage() { (page + 1) * pageSize + 1 <= getItemsCount() }

    void nextPage() { if (isHasNextPage()) page++ }

    Boolean isHasPreviousPage() { page > 0 }

    void previousPage() { if (isHasPreviousPage()) page-- }
}

class DomainControllerConverter<M extends Domain, ID extends Serializable> implements Converter {

    static simpleInstanceName = { ->
        def sb = new StringBuilder(M.simpleName)
        sb.replace 0, 1, new String(sb.toString().charAt(0)).toLowerCase()
        sb.toString()
    }

    Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
        value == null || value?.length() == 0 ? null : (facesContext.application.ELResolver.getValue(facesContext.ELContext, null, "$simpleInstanceName${Controller.simpleName}") as AbstractDomainController).getModel(M.newInstance().parseId(value) as ID)
    }

    String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null) null
        if (object instanceof M) object.id.toString()
        else throw new IllegalArgumentException("object " + object + " is of type " + object.class.name + " expected type: " + M.name)
    }
}
