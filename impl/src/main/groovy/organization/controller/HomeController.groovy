package organization.controller

import org.springframework.stereotype.Controller

//import javax.enterprise.context.RequestScoped
import javax.faces.context.FacesContext
import javax.inject.Named

@Controller
//@RequestScoped
@Named('homeController')
class HomeController {

    def getSayHello() { "Hello from ${this.class.simpleName}" }

    def getLocaleLanguage() { FacesContext.currentInstance.externalContext.requestLocale.language }

    def getLoremIpsum() {
        """                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consequat tellus eu dui luctus
                        in rhoncus ipsum ultrices. Nunc ultricies pharetra nunc, eu feugiat dolor pretium sit amet. Nulla
                        sed mauris sed arcu aliquam suscipit vitae vitae enim. Nunc vitae enim velit. Nulla facilisi. Duis
                        ligula ligula, ullamcorper eu faucibus quis, commodo ut sem. Vivamus cursus faucibus lorem, in
                        bibendum ante rutrum non. Maecenas et arcu libero, sit amet malesuada leo. Morbi ac libero sed
                        tellus blandit feugiat. Proin turpis lectus, lacinia sed laoreet in, posuere vel dui. Maecenas
                        sollicitudin felis et massa cursus eget tempor nulla porta. Donec venenatis auctor ultrices.
                        Vestibulum elit augue, lobortis condimentum vulputate pulvinar, placerat vitae lectus. Donec quis
                        est ut dolor eleifend commodo at ac nibh. Fusce eleifend consequat nulla eu sagittis."""
    }
}
