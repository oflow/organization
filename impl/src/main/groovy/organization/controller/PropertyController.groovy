package organization.controller

import organization.repository.Property
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

import javax.faces.convert.FacesConverter
import javax.inject.Named

@Controller
@RequestMapping('/property')
@Named('propertyController')
class PropertyController extends AbstractDomainController<Property, Long> {
    PropertyController() { super(Property) }

    @RequestMapping
    String prepareList() { Object.prepareList() }

    @RequestMapping
    String prepareView() { Object.prepareEdit() }

    @RequestMapping
    String prepareCreate() { Object.prepareCreate() }

    @RequestMapping
    String prepareEdit() { Object.prepareEdit() }

    @RequestMapping
    String update() { Object.update() }

    @RequestMapping
    String destroy() { Object.destroy() }

    @RequestMapping
    String destroyAndView() { Object.destroyAndView() }

    @RequestMapping
    String next() { Object.next() }

    @RequestMapping
    String previous() { Object.previous() }


}

@FacesConverter(forClass = Property)
class PropertyControllerConverter extends DomainControllerConverter<Property, Long> {

}
