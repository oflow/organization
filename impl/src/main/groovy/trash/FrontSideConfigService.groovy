package trash

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.inject.Named

@Service
@Transactional
@Default
@FrontSideConfigServiceQualifier
@Named('frontSideConfigService')
class FrontSideConfigService extends AbstractConfigService implements IFrontSideConfigService {
    @Inject
    @I18nConfigServiceQualifier
    II18nConfigService i18nConfigService
    @Inject
    @SmtpConfigServiceQualifier
    ISmtpConfigService smtpConfigService


    def load() {
        i18nConfigService.load()
        smtpConfigService.load()
    }
}