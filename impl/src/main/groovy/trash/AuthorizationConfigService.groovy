package trash

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.enterprise.inject.Default
import javax.inject.Named
@Service
@Transactional
@Default
@AuthorizationConfigServiceQualifier
@Named('authorizationConfigService')
class AuthorizationConfigService extends AbstractConfigService implements IAuthorizationConfigService {

    def load() {}
}
