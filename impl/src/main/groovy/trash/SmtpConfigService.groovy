package trash

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import organization.repository.dao.IPropertyDao
import organization.repository.dao.PropertyDao

import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.inject.Named

@Service
@Transactional
@Default
@SmtpConfigServiceQualifier
@Named("smtpConfigService")
class SmtpConfigService extends AbstractConfigService implements ISmtpConfigService {

    @Inject
    @PropertyDao
    IPropertyDao propertyDao


    def load() {
        propertyDao.createOrUpdate MAIL_SMTP_HOST_KEY, MAIL_SMTP_HOST_KEY, false
        propertyDao.createOrUpdate MAIL_SMTP_PORT_KEY, MAIL_SMTP_PORT_DEFAULT_VALUE.toString(), false
        propertyDao.createOrUpdate MAIL_SMTP_USER_KEY, MAIL_SMTP_USER_KEY, false
        propertyDao.createOrUpdate MAIL_SMTP_PASSWORD_KEY, MAIL_SMTP_PASSWORD_KEY, false
        propertyDao.createOrUpdate MAIL_SMTP_FROM_KEY, MAIL_SMTP_FROM_KEY, false
        propertyDao.createOrUpdate MAIL_SMTP_STARTTLS_PORT_KEY, MAIL_SMTP_STARTTLS_PORT_KEY, false
        propertyDao.createOrUpdate MAIL_SMTP_STARTTLS_ENABLE_KEY, MAIL_SMTP_STARTTLS_ENABLE_KEY, false
    }

    @Override
    def update(Map<String, Object> map) {
        map.each {
            switch (it.key) {
                case MAIL_SMTP_HOST_KEY: propertyDao.createOrUpdate MAIL_SMTP_HOST_KEY, map[MAIL_SMTP_HOST_KEY], true; break
                case MAIL_SMTP_PORT_KEY: propertyDao.createOrUpdate MAIL_SMTP_PORT_KEY, map[MAIL_SMTP_PORT_KEY], true; break
                case MAIL_SMTP_USER_KEY: propertyDao.createOrUpdate MAIL_SMTP_USER_KEY, map[MAIL_SMTP_USER_KEY], true; break
                case MAIL_SMTP_PASSWORD_KEY: propertyDao.createOrUpdate MAIL_SMTP_PASSWORD_KEY, map[MAIL_SMTP_PASSWORD_KEY], true; break
                case MAIL_SMTP_FROM_KEY: propertyDao.createOrUpdate MAIL_SMTP_FROM_KEY, map[MAIL_SMTP_FROM_KEY], true; break
                case MAIL_SMTP_STARTTLS_PORT_KEY: propertyDao.createOrUpdate MAIL_SMTP_STARTTLS_PORT_KEY, map[MAIL_SMTP_STARTTLS_PORT_KEY], true; break
                case MAIL_SMTP_STARTTLS_ENABLE_KEY: propertyDao.createOrUpdate MAIL_SMTP_STARTTLS_ENABLE_KEY, map[MAIL_SMTP_STARTTLS_ENABLE_KEY], true; break
            }
        }
    }
}
