package trash

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.enterprise.inject.Default
import javax.inject.Named
@Service
@Transactional
@Default
@OrganizationConfigServiceQualifier
@Named('organizationConfigService')
class OrganizationConfigService extends AbstractConfigService implements IOrganizationConfigService {

    //    IAuthorizationConfigService authorizationConfigService
    //    IRegisterConfigService registrationConfigService


    def load() {}
}
