package trash

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import javax.enterprise.inject.Default
import javax.inject.Named
@Service
@Transactional
@Default
@RegisterConfigServiceQualifier
@Named('registerConfigService')
class RegisterConfigService extends AbstractConfigService implements IRegisterConfigService {

    def load() {}
}
