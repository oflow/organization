package trash

import organization.repository.dao.ILangDao
import organization.repository.dao.LangDao
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import organization.repository.LangDomain

import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.inject.Named

@Service
@Transactional
@Default
@I18nConfigServiceQualifier
@Named('i18nConfigService')
class I18nConfigService extends AbstractConfigService implements II18nConfigService {

    //todo:feeds value with groovy.util.ConfigObject constructed with conf files
    static final CODES = [
            II18nConfigService.LANG_CODE_DEFAULT,
            II18nConfigService.LANG_CODE_EN,
            II18nConfigService.LANG_CODE_FR
    ] as String[]

    @Inject
    @LangDao
    ILangDao langDao

    def load() {
        CODES.each {
            assert langDao.addSupportedLang(it)
                    .properties[LangDomain.ID_FIELD]
        }
    }
}