package trash

import org.junit.Test

class AbstractConfigServiceTest {

    class DummyConfigService extends AbstractConfigService {
        @Override
        def load() {}
    }

    @Test
    void testUnTruc() {
        DummyConfigService applicationConfigService = new DummyConfigService()
        assert !applicationConfigService.applicationConfig
    }
}

