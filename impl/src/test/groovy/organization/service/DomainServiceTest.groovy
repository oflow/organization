package organization.service

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.test.context.ContextConfiguration
import organization.AbstractCoreTests
import organization.repository.Property

import javax.inject.Inject

@ContextConfiguration(locations = ["classpath:META-INF/spring/organization/service/organization-service_applicationContext-test.xml"])
class DomainServiceTest extends AbstractCoreTests {

    @Inject
    @DomainServiceQualifier
    IDomainService domainService

    @Before
    void setUp() {

    }

    @After
    void tearDown() {

    }

    @Test
    void testSave() {

    }

    @Test
    void testDelete() {

    }

    @Test
    void testUpdateAll() {

    }

    @Test
    void testSaveAll() {

    }

    @Test
    void testDeleteAll() {

    }

    @Test
    void testFind() {

    }

    @Test
    void testFindAll() {
//        domainService.findAll(Property).each { println(it) }
        List result = domainService.findAll(Property)
        Integer propertyCount = countEntity(Property)
        Long id = 1
        propertyCount.times {
            assert result.get(id - 1 as Integer) == em.find(Property, id)
            id++
        }
    }

    @Test
    void testFindAll1() {

    }

    @Test
    void testCount() {

    }
}
