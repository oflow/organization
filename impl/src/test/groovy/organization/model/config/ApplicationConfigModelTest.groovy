package organization.model.config

import groovy.util.logging.Slf4j
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.junit.Before
import org.junit.Test

import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

@Slf4j
class ApplicationConfigModelTest {
//todo: coder en premier
    ApplicationConfigModel cm

    @Before
    void setUp() {
        cm = new ApplicationConfigModel(
            siteConfigModel: new SiteConfigModel(
                name: "fooModel",
                siteAdminEmailValue: "admin@acme.com",
                siteAdminPassword: "admin.password",
                siteAdminUsername: "admin",
                siteName: "Site",
                siteUrl: "http://localhost:8080/web"),
            dataSourceConfigModelMap: [new DataSourceConfigModel(
                    name: "fooModel",
                    database: "MYSQL",
                    ddl: "update",
                    dialect: "dummy.dialect",
                    driverClassName: "foo.Bar.Class",
                    password: "fooBar",
                    showSql: true,
                    url: "http://www.acme.com",
                    username: "john.doe")],
            mailConfigModels: [new MailConfigModel(
                    name: "fooModel",
                    mailSmtpFrom: "john.doe@acme.com",
                    mailSmtpHost: "smtp.acme.com",
                    mailSmtpPassword: "fooBar",
                    mailSmtpPort: "25",
                    mailSmtpStarttlsEnable: Boolean.TRUE,
                    mailSmtpTlsStarttlsPort: "25",
                    mailSmtpUser: "john.doe")])
    }


    @Test
    void testValidation() {
        assert cm.isValid()
    }

    @Test
    void testJAXB() {
        //creation of the xsd and copy into resources folders
        //and copy of the jar in the griffon app
        JAXBContext context = JAXBContext.newInstance(ApplicationConfigModel.class)

        //some tests to play with JAXB and JSON
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal cm, writer
        String configurationModelXml = writer.toString()
        writer.flush()
        writer.close()
        log.info configurationModelXml

        ObjectMapper mapper = new ObjectMapper(
            annotationIntrospector: new JaxbAnnotationIntrospector())
        String configurationModelJSon = mapper.writeValueAsString(cm)
        log.info configurationModelJSon
        log.info mapper.readValue(configurationModelJSon, ApplicationConfigModel.class).toString()

        log.info cm.toString()
        log.info cm.toXml()
        log.info cm.toJson()

        assert cm == cm.parseJson(cm.toJson())
    }
}
