package organization.repository

import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.validation.Validation
import javax.validation.Validator

class HttpHeaderTest {

    static Validator validator

    @BeforeClass
    static void setUpClass() {
        validator = Validation.buildDefaultValidatorFactory().validator
    }

    HttpHeader header

    @Before
    void setUp() {
        header = new HttpHeader(header: 'foo')
    }

    @Test
    void testBeanValidation() {
        assert validator.validate(header).empty
    }
}