package organization.repository

import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.NotBlank
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

class BrowserTest {
    static Validator validator

    @BeforeClass
    static void setUpClass() {
        validator = Validation.buildDefaultValidatorFactory().validator
    }

    Browser browser, other, copy, detached

    @Before
    void setUp() {
        browser = new Browser(id: 1L, browserName: "Firefoox", browserVersion: "8.0", browserOs: 'foo')
        other = new Browser(id: 2L, browserName: "Foopera", browserVersion: "5.0", browserOs: 'bar')
        copy = new Browser(id: 1L, browserName: "Firefoox", browserVersion: "8.0", browserOs: 'foo')
        detached = new Browser(browserName: "Firefoox", browserVersion: "8.0", browserOs: 'foo')
    }

    @Test
    void testHashCode() {
        assert browser.hashCode()
        assert browser.hashCode() != other.hashCode()
        assert browser.hashCode() == copy.hashCode()
        assert browser.hashCode() == detached.hashCode()
    }

    @Test
    void testEquals() {
        assert browser
        assert browser != other
        assert browser == copy
        assert browser == detached
    }

    @Test
    void testToXml() {
        JAXBContext context = JAXBContext.newInstance(Browser)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal browser, writer
        String browserXml = writer.toString()
        writer.flush(); writer.close()
        assert browserXml.contains(browser.id.toString())
        assert browserXml.contains(browser.browserName)
        assert browserXml.contains(browser.browserVersion)
        assert browserXml.contains(browser.browserOs)
        assert browserXml == browser.toXml()
    }

    @Test
    void testParseXml() {
        JAXBContext context = JAXBContext.newInstance(Browser)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal browser, writer
        String browserXml = writer.toString()
        writer.flush(); writer.close()
        assert browser == Browser.parseXml(browserXml)
    }

    @Test
    void testToJSon() {
        ObjectMapper mapper = new ObjectMapper(
            annotationIntrospector: new JaxbAnnotationIntrospector())
        String browserJson = mapper.writeValueAsString(browser)
        assert browserJson.contains(browser.id.toString())
        assert browserJson.contains(browser.browserName)
        assert browserJson.contains(browser.browserVersion)
        assert browserJson.contains(browser.browserOs)
        assert browserJson == browser.toJson()
    }

    @Test
    void testParseJson() {
        ObjectMapper mapper = new ObjectMapper(
            annotationIntrospector: new JaxbAnnotationIntrospector())
        String browserJson = mapper.writeValueAsString(browser)

        assert browserJson.contains(browser.id.toString())
        assert browserJson.contains(browser.browserName)
        assert browserJson.contains(browser.browserVersion)
        assert browserJson.contains(browser.browserOs)
        Browser retr = mapper.readValue(browserJson, Browser)

        assert browser == retr
        assert retr == Browser.parseJson(browserJson)
    }

    @Test
    void testBrowserVersionNotBlankConstraints() {
        assert browser.browserVersion
        assert validator.validateProperty(browser,
            Browser.VERSION_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case null
        browser.browserVersion = null
        constraintViolations =
        validator.validateProperty(browser,
            Browser.VERSION_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first()
        .constraintDescriptor
        .annotation
        .annotationType() == NotBlank
        //case blank
        browser.browserVersion = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.VERSION_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testBrowserVersionSizeConstraints() {
        assert browser.browserVersion
        assert browser.browserVersion.size() >= 1
        assert browser.browserVersion.size() <= 255
        assert validator.validateProperty(browser,
            Browser.VERSION_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case less than min
        browser.browserVersion = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.VERSION_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == Size) ? true : null
        }

        //case more than max
        browser.browserVersion = UUID.randomUUID().toString().replaceAll('-', '')
        5.times { browser.browserVersion += browser.browserVersion }
        constraintViolations = validator.validateProperty(browser, Browser.VERSION_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>).constraintDescriptor
                .annotation.annotationType() == Size) ? true : null
        }
    }

    @Test
    void testNameNotBlankConstraints() {
        assert browser.browserName
        assert validator.validateProperty(browser,
            Browser.NAME_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case null
        browser.browserName = null
        constraintViolations =
        validator.validateProperty(browser,
            Browser.NAME_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first()
        .constraintDescriptor
        .annotation
        .annotationType() == NotBlank
        //case blank
        browser.browserName = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.NAME_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testBrowserNameSizeConstraints() {
        assert browser.browserName
        assert browser.browserName.size() >= 1
        assert browser.browserName.size() <= 255
        assert validator.validateProperty(browser,
            Browser.NAME_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case less than min
        browser.browserName = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.NAME_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == Size) ? true : null
        }

        //case more than max
        browser.browserName = UUID.randomUUID().toString().replaceAll('-', '')
        5.times { browser.browserName += browser.browserName }
        constraintViolations = validator
        .validateProperty(browser, Browser.NAME_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == Size) ? true : null
        }
    }

    @Test
    void testOsNotBlankConstraints() {
        assert browser.browserOs
        assert validator.validateProperty(browser, Browser.OS_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case null
        browser.browserOs = null
        constraintViolations =
        validator.validateProperty(browser,
            Browser.OS_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first()
        .constraintDescriptor
        .annotation
        .annotationType() == NotBlank
        //case blank
        browser.browserOs = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.OS_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testOsSizeConstraints() {
        assert browser.browserOs
        assert browser.browserOs.size() >= 1
        assert browser.browserOs.size() <= 255
        assert validator.validateProperty(browser,
            Browser.OS_FIELD).empty
        Set<ConstraintViolation<Browser>> constraintViolations
        //case less than min
        browser.browserOs = ''
        constraintViolations = validator
        .validateProperty(browser,
            Browser.OS_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == Size) ? true : null
        }

        //case more than max
        browser.browserOs = UUID.randomUUID().toString().replaceAll('-', '')
        5.times { browser.browserOs += browser.browserOs }
        constraintViolations = validator
        .validateProperty(browser, Browser.OS_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Browser>)
                .constraintDescriptor.annotation
                .annotationType() == Size) ? true : null
        }
    }
}