package organization.repository

import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.NotBlank
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

class PropertyTest {
    static Validator validator

    @BeforeClass
    static void setUpClass() {
        validator = Validation.buildDefaultValidatorFactory().validator
    }

    Property property, other, copy, detached

    @Before
    void setUp() {
        property = new Property(id: 1L, key: 'foo', value: 'bar', version: 0)
        copy = new Property(id: 1L, key: 'foo', value: 'bar', version: 0)
        detached = new Property(key: 'foo', value: 'bar')
        other = new Property(id: 2L, key: 'baz', value: 'qux', version: 0)
    }

    @Test
    void testKeyToLowerCase() {
        property.key = property.key.toUpperCase()
        assert property.key == 'FOO'
        property.toLowerCase()
        assert property.key == 'FOO'.toLowerCase()
    }

    @Test
    void testHashCode() {
        assert property.hashCode()
        assert property.hashCode() != other.hashCode()
        assert property.hashCode() == copy.hashCode()
        assert property.hashCode() == detached.hashCode()
    }

    @Test
    void testEquals() {
        assert property
        assert property != other
        assert property == copy
        assert property == detached
    }

    @Test
    void testToXml() {
        JAXBContext context = JAXBContext.newInstance(Property)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal property, writer
        String propertyXml = writer.toString()
        writer.flush(); writer.close()
        assert propertyXml.contains(property.id.toString())
        assert propertyXml.contains(property.key)
        assert propertyXml.contains(property.value)
        assert propertyXml == property.toXml()
    }

    @Test
    void testParseXml() {
        JAXBContext context = JAXBContext.newInstance(Property)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal property, writer
        String propertyXml = writer.toString()
        writer.flush(); writer.close()
        assert property == Property.parseXml(propertyXml)
    }

    @Test
    void testToJSon() {
        ObjectMapper mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        String propertyJson = mapper.writeValueAsString(property)
        assert propertyJson.contains(property.id.toString())
        assert propertyJson.contains(property.key)
        assert propertyJson.contains(property.value)
        assert propertyJson == property.toJson()
    }

    @Test
    void testParseJson() {
        ObjectMapper mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        String propertyJson = mapper.writeValueAsString(property)

        assert propertyJson.contains(property.id.toString())
        assert propertyJson.contains(property.key)
        assert propertyJson.contains(property.value)
        Property retr = mapper.readValue(propertyJson, Property)

        assert property == retr
        assert retr == Property.parseJson(propertyJson)
    }

    @Test
    void testKeyNotBlankConstraints() {
        assert property.key
        assert validator.validateProperty(property, Property.KEY_FIELD).empty
        Set<ConstraintViolation<Property>> constraintViolations
        //case null
        property.key = null
        constraintViolations = validator.validateProperty(property, Property.KEY_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first().constraintDescriptor.annotation.annotationType() == NotBlank
        //case blank
        property.key = ''
        constraintViolations = validator.validateProperty(property, Property.KEY_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testKeySizeConstraints() {
        assert property.key
        assert property.key.size() >= 1
        assert property.key.size() <= 255
        assert validator.validateProperty(property, Property.KEY_FIELD).empty
        Set<ConstraintViolation<Property>> constraintViolations
        //case less than min
        property.key = ''
        constraintViolations = validator.validateProperty(property, Property.KEY_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == Size) ? true : null
        }

        //case more than max
        property.key = UUID.randomUUID().toString().replaceAll('-', '')
        5.times { property.key += property.key }
        constraintViolations = validator.validateProperty(property, Property.KEY_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == Size) ? true : null
        }
    }

    @Test
    void testValueNotBlankConstraints() {
        assert property.value
        assert validator.validateProperty(property, Property.VALUE_FIELD).empty
        Set<ConstraintViolation<Property>> constraintViolations
        //case null
        property.value = null
        constraintViolations = validator.validateProperty(property, Property.VALUE_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first().constraintDescriptor.annotation.annotationType() == NotBlank
        //case blank
        property.value = ''
        constraintViolations = validator.validateProperty(property, Property.VALUE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testValueSizeConstraints() {
        assert property.value
        assert property.value.size() >= 1
        assert property.value.size() <= 4096
        assert validator.validateProperty(property, Property.VALUE_FIELD).empty
        Set<ConstraintViolation<Property>> constraintViolations
        //case less than min
        property.value = ''
        constraintViolations = validator.validateProperty(property, Property.VALUE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == Size) ? true : null
        }

        //case more than max
        property.value = UUID.randomUUID().toString().replaceAll('-', '')
        8.times { property.value += property.value }
        constraintViolations = validator.validateProperty(property, Property.VALUE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>).constraintDescriptor.annotation.annotationType() == Size) ? true : null
        }
    }
}
