package organization.repository.dao

import com.mdimension.jchronic.Chronic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener
import org.springframework.transaction.annotation.Transactional
import organization.CleanDataInsertTestExecutionListener
import organization.IDataSetLocator
import organization.repository.Domain

import javax.enterprise.inject.Default
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Qualifier
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import java.lang.annotation.Retention
import java.lang.annotation.Target

import static java.lang.annotation.ElementType.*
import static java.lang.annotation.RetentionPolicy.RUNTIME
import static javax.persistence.TemporalType.*

@Table(name = "foo")
@Entity
@ToString(includeNames = true)
@EqualsAndHashCode(excludes = ['id', 'version'])
class Foo implements Domain<Long> {
    Long parseId(String s) { Long.valueOf(s) }
    @Id
    @GeneratedValue
    Long id
    @Version
    Integer version
    @NotNull
    @Size(min = 1, max = 6)
    String bar
    @Column(unique = true)
    Long baz
    @Temporal(TIMESTAMP)
    @NotNull
    Date startDate
}


interface IFooDao extends Dao<Foo, Long> {
    def findByBaz(Long baz)

    def findByBarLikeAndBetweenBaz(Map<String, Object> params)

    def findByBarLikeAndBetweenBaz(Map<String, Object> params, Integer[] range)

    def findByBarLike(String bar)

    def findByStartDate(Date date)

    def findBetweenStartDates(Date date1, Date date2)
}

@Qualifier
@Retention(RUNTIME)
@Target([METHOD, FIELD, PARAMETER, TYPE])
@interface FooDao {}

@Service
@Transactional
@Default
@FooDao
@Named('fooDao')
class FooDaoJpa extends DaoJpa<Foo, Long> implements IFooDao {

    FooDaoJpa() { super(Foo) }

    Foo save(Foo model) { super.save model }

    List<Foo> saveAll(Collection<Foo> models) { super.saveAll(models) }

    void delete(Foo foo) { super.delete(foo) }

    @Transactional(readOnly = true)
    Foo find(Long id) { super.find id }

    @Transactional(readOnly = true)
    List<Foo> findAll() { super.findAll() }

    @Transactional(readOnly = true)
    List<Foo> findAll(Integer[] range) { super.findAll(range) }

    @Transactional(readOnly = true)
    Integer count() { super.count() }

    void deleteAll(Collection<Foo> models) { super.deleteAll(models) }

    List<Foo> updateAll(Collection<Foo> models) { super.updateAll(models) }

    @Transactional(readOnly = true)
    def findByBarLikeAndBetweenBaz(Map<String, Object> params) {
        executeQuery 'from Foo f where f.bar like :bar and baz between :baz1 and :baz2 order by f.id asc', params
    }

    @Transactional(readOnly = true)
    def findByBarLikeAndBetweenBaz(Map<String, Object> params, Integer[] range) {
        executeQuery 'from Foo f where f.bar like :bar and baz between :baz1 and :baz2 order by f.id asc',
                params,
                range
    }

    @Transactional(readOnly = true)
    def findByBaz(Long baz) {
        executeQuery 'from Foo f where f.baz = :baz', ['baz': baz]
    }

    @Transactional(readOnly = true)
    def findByBarLike(String bar) {
        executeQuery("select f from Foo f where f.bar like :bar", ['bar': bar])
    }

    @Transactional(readOnly = true)
    def findByStartDate(Date date) {
        executeQuery('select f from Foo f where f.startDate = :startDate', ['startDate': date])
    }

    @Transactional(readOnly = true)
    def findBetweenStartDates(Date date1, Date date2) {
        executeQuery('select f from Foo f where f.startDate between :date1 and :date2',
                ['date1': date1, 'date2': date2])
    }
}


@RunWith(SpringJUnit4ClassRunner)
@ContextConfiguration(locations = ['classpath:META-INF/spring/organization/dao/jpa/organization-dao-jpa_applicationContext-test.xml'])
@TestExecutionListeners([CleanDataInsertTestExecutionListener, DependencyInjectionTestExecutionListener])
class DaoJpaTests implements IDataSetLocator {

    @Value('${database.dataSet}')
    String dataSet

    @Inject
    @FooDao
    IFooDao fooDao

    @PersistenceContext
    EntityManager em

    Integer getCountFoo() {
        em.createQuery('select count(*) from Foo f', Long).singleResult.intValue()
    }

    @Ignore
    @Test
    void testSaveOnCreate() {
        Integer cpt = countFoo
        Foo foo = new Foo(bar: 'bar', baz: 0L, startDate: new Date())
        assert !foo.id
        assert fooDao.save(foo).id
        assert countFoo == cpt + 1
    }

    @Ignore
    @Test
    void testSaveOnUpdate() {
        Foo foo = em.find(Foo, 1L)
        String bar = foo.bar
        foo.bar = DaoJpaTests.name.substring(0, 4)
        fooDao.save foo
        assert em.find(Foo, 1L).bar != bar
        assert em.find(Foo, 1L).bar == DaoJpaTests.name.substring(0, 4)
    }

    @Ignore
    @Test
    void testFind() {
        assert em.find(Foo, 1L) == fooDao.find(1L)
    }

    @Ignore
    @Test
    void testDelete() {
        Integer cpt = countFoo
        fooDao.delete em.find(Foo, 1L)
        assert cpt - 1 == countFoo
    }


    @Ignore
    @Test
    void testFindAll_0args() {
        List<Foo> foos = em.createQuery('from Foo', Foo).resultList
        List<Foo> result = fooDao.findAll()
        assert result.size() == foos.size()
        result.eachWithIndex { obj, i -> assert obj == foos.get(i) }
    }

    @Ignore
    @Test
    void testFindAll_1args() {
        List<Foo> foos = em.createQuery('from Foo', Foo).resultList
        List<Foo> result = fooDao.findAll()
        assert result.size() == foos.size()
        result.eachWithIndex { foo, i -> assert foo == foos.get(i) }
    }

    @Ignore
    @Test
    void testFindByBaz() {
        assert fooDao.findByBaz(1L) ==
                em.createQuery('from Foo f where f.baz = :baz', Foo)
                        .setParameter('baz', 1L).singleResult
    }

    @Ignore
    @Test
    void testFindByBarLikeAndBetweenBaz_1args() {
        Map<String, Object> params = ['bar': 'bar0%', 'baz1': 11L, 'baz2': 20L]
        Query q = em.createQuery('from Foo f where ' +
                'f.bar like :bar and ' +
                'f.baz between :baz1 and :baz2 ' +
                'order by f.id asc', Foo)
        params.each { q.setParameter(it.key, it.value) }
        def expected = q.resultList
        def result = fooDao.findByBarLikeAndBetweenBaz(params)
        assert result.size() == expected.size()
        result.eachWithIndex { obj, i ->
            assert obj == expected.get(i)
        }
    }

    @Ignore
    @Test
    void testFindByBarLikeAndBetweenBaz_2args() {
        Map<String, Object> params = ['bar': 'bar0%', 'baz1': 11L, 'baz2': 20L]
        Integer[] range = [0, 4]
        Query q = em.createQuery('from Foo f where ' +
                'f.bar like :bar and ' +
                'f.baz between :baz1 and :baz2 ' +
                'order by f.id asc', Foo)
        params.each { q.setParameter(it.key, it.value) }
        q.firstResult = range.first()
        q.maxResults = range.last()
        List<Foo> expected = q.resultList
        def result = fooDao.findByBarLikeAndBetweenBaz(params, range)
        assert result.size() == expected.size()
        result.eachWithIndex { foo, i ->
            assert foo == expected.get(i)
        }
    }

    @Ignore
    @Test
    void testFindByBarLike() {
        String barLike = '%bar0%'
        List<Foo> expected = em.createQuery(
                "from Foo f where f.bar like :bar", Foo)
                .setParameter('bar', barLike)
                .resultList
        def result = fooDao.findByBarLike(barLike)
        assert expected.size() == result.size()
        expected.eachWithIndex { foo, i ->
            assert foo == result.get(i)
        }
    }

    @Ignore
    @Test
    void testFindByStartDate() {
        Date date = Chronic.parse('2011-01-01 12:00:00').endCalendar.time
        assert em.createQuery(
                "select f from Foo f where f.startDate = :startDate", Foo)
                .setParameter('startDate', date)
                .singleResult == fooDao.findByStartDate(date)
    }

    @Ignore
    @Test
    void testFindBetweenStartDates() {
        Date date1 = Chronic.parse('2011-01-01 12:00:00').endCalendar.time
        Date date2 = Chronic.parse('2011-01-10 12:00:00').endCalendar.time
        Query q = em.createQuery("select f from Foo f where " +
                "f.startDate between :date1 and :date2", Foo)
        q.setParameter 'date1', date1
        q.setParameter 'date2', date2
        List<Foo> expected = q.resultList
        def result = fooDao.findBetweenStartDates(date1, date2)
        assert expected.size() == result.size()
        result.eachWithIndex { foo, i -> assert foo == expected.get(i) }
    }
}