package organization.repository.dao

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.SingleConnectionDataSource
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import organization.DbUnitHelper

import javax.sql.DataSource

@Configuration
class CoreDaoAppConfig {
    @Value('${database.driverClassName}')
    String databaseDriverClassName
    @Value('${database.url}')
    String databaseUrl
    @Value('${database.username}')
    String databaseUsername
    @Value('${database.password}')
    String databasePassword
    @Value('${database.showSql}')
    String databaseShowSql
    @Value('${database.database}')
    String databaseDatabase
    @Value('${database.hibernate.dialect}')
    String databaseHibernateDialect
    @Value('${database.hibernate.hbm2ddl.auto}')
    String databaseHibernateHbm2ddlAuto

    @Bean
    DataSource dataSource() {
        new SingleConnectionDataSource(
            driverClassName: databaseDriverClassName,
            url: databaseUrl,
            username: databaseUsername,
            password: databasePassword,
            suppressClose: true)
    }

    @Bean
    DbUnitHelper dbUnitHelper() {
        new DbUnitHelper(dataSource: dataSource())
    }

    @Bean
    HibernateJpaVendorAdapter jpaVendorAdapter() {
        new HibernateJpaVendorAdapter(
            showSql: Boolean.parseBoolean(databaseShowSql),
            database: databaseDatabase,
            databasePlatform: databaseHibernateDialect)
    }
}

