package organization.repository.dao

import org.junit.Test
import org.springframework.test.context.ContextConfiguration
import organization.AbstractCoreTests
import organization.repository.Lang

import javax.inject.Inject

@ContextConfiguration(locations = ["classpath:META-INF/spring/organization/dao/organization-dao_applicationContext-test.xml"])
class LangDaoJpaTest extends AbstractCoreTests {

    @Inject
    @LangDao
    ILangDao langDao

    @Test
    void testAddSupportedLang() {
        //case : existing supported lang
        def langCount = countEntity(Lang)
        def map = langDao.addSupportedLang(Locale.ENGLISH.language)
        assert map['id']
        assert map['supported']
        assert langCount == countEntity(Lang)
        //case : existing not supported lang
        Lang l = em.createQuery('from Lang l where l.supported is not true', Lang)
                .resultList.first()
        assert l
        assert !l.supported
        map = langDao.addSupportedLang(l.code)
        assert map['id']
        assert map['supported']
        assert langCount == countEntity(Lang)
        //case : not existing lang
        def code = 'foo'
        assert em.createQuery("from Lang l where l.code = '${code}'")
                .resultList.isEmpty()
        map = langDao.addSupportedLang('foo')
        assert map['id']
        assert map['supported']
        assert langCount + 1 == countEntity(Lang)
    }

}