package organization.repository

import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector
import org.hibernate.validator.constraints.NotBlank
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

class LangTest {
    static Validator validator

    @BeforeClass
    static void setUpClass() {
        validator = Validation.buildDefaultValidatorFactory().validator
    }

    Lang lang, other, copy, detached

    @Before
    void setUp() {
        lang = new Lang(id: 1L, code: Locale.ENGLISH.language, supported: true)
        copy = new Lang(id: 1L, code: Locale.ENGLISH.language, supported: true)
        detached = new Lang(code: Locale.ENGLISH.language, supported: true)
        other = new Lang(id: 2L, code: Locale.FRENCH.language, supported: true)
    }

    @Test
    void testToLowerCase() {
        lang.code = lang.code.toUpperCase()
        assert lang.code == Locale.ENGLISH.language.toUpperCase()
        lang.toLowerCase()
        assert lang.code == Locale.ENGLISH.language.toLowerCase()
    }

    @Test
    void testHashCode() {
        assert lang.hashCode()
        assert lang.hashCode() != other.hashCode()
        assert lang.hashCode() == copy.hashCode()
        assert lang.hashCode() == detached.hashCode()
    }

    @Test
    void testEquals() {
        assert lang
        assert lang != other
        assert lang == copy
        assert lang == detached
    }

    @Test
    void testToXml() {
        JAXBContext context = JAXBContext.newInstance(Lang)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal lang, writer
        String langXml = writer.toString()
        writer.flush(); writer.close()
        assert langXml.contains(lang.id.toString())
        assert langXml.contains(lang.code)
        assert langXml.contains(lang.supported.toString())
        assert langXml == lang.toXml()
    }

    @Test
    void testParseXml() {
        JAXBContext context = JAXBContext.newInstance(Lang)
        Marshaller m = context.createMarshaller()
        m.setProperty Marshaller.JAXB_FORMATTED_OUTPUT, true
        StringWriter writer = new StringWriter()
        m.marshal lang, writer
        String langXml = writer.toString()
        writer.flush(); writer.close()
        assert lang == Lang.parseXml(langXml)
    }

    @Test
    void testToJSon() {
        ObjectMapper mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        String langJson = mapper.writeValueAsString(lang)
        assert langJson.contains(lang.id.toString())
        assert langJson.contains(lang.code)
        assert langJson.contains(lang.supported.toString())
        assert langJson == lang.toJson()
    }

    @Test
    void testParseJson() {
        ObjectMapper mapper = new ObjectMapper(
                annotationIntrospector: new JaxbAnnotationIntrospector())
        String langJson = mapper.writeValueAsString(lang)

        assert langJson.contains(lang.id.toString())
        assert langJson.contains(lang.code)
        assert langJson.contains(lang.supported.toString())
        Lang retr = mapper.readValue(langJson, Lang)

        assert lang == retr
        assert retr == Lang.parseJson(langJson)
    }

    @Test
    void testCodeNotBlankConstraints() {
        assert lang.code
        assert validator.validateProperty(lang, Lang.CODE_FIELD).empty
        Set<ConstraintViolation<Lang>> constraintViolations
        //case null
        lang.code = null
        constraintViolations =
                validator.validateProperty(lang, Lang.CODE_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first()
                .constraintDescriptor
                .annotation
                .annotationType() == NotBlank
        //case blank
        lang.code = ''
        constraintViolations = validator
                .validateProperty(lang, Lang.CODE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Lang>)
                    .constraintDescriptor.annotation
                    .annotationType() == NotBlank) ? true : null
        }
    }

    @Test
    void testCodeSizeConstraints() {
        assert lang.code
        assert lang.code.size() >= 1
        assert lang.code.size() <= 8
        assert validator.validateProperty(lang, Lang.CODE_FIELD).empty
        Set<ConstraintViolation<Lang>> constraintViolations
        //case less than min
        lang.code = ''
        constraintViolations = validator
                .validateProperty(lang, Lang.CODE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Property>)
                    .constraintDescriptor.annotation
                    .annotationType() == Size) ? true : null
        }

        //case more than max
        lang.code = UUID.randomUUID().toString().replaceAll('-', '')
        constraintViolations = validator.validateProperty(lang, Lang.CODE_FIELD)
        assert constraintViolations.findResult {
            ((it as ConstraintViolation<Lang>).constraintDescriptor
                    .annotation.annotationType() == Size) ? true : null
        }
    }

    @Test
    void testSupportedNotNullConstraints() {
        assert lang.supported
        assert validator.validateProperty(lang, Lang.SUPPORTED_FIELD).empty
        Set<ConstraintViolation<Lang>> constraintViolations
        //case null
        lang.supported = null
        constraintViolations = validator.validateProperty(lang,
                Lang.SUPPORTED_FIELD)
        assert constraintViolations.size() == 1
        assert constraintViolations.first()
                .constraintDescriptor
                .annotation
                .annotationType() == NotNull
    }
}
