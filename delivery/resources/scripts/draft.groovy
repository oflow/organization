import core.Lang

//def shell = new GroovyShell(classLoader, new Binding(ctx: appCtx, grailsApplication: grailsApp))
//appCtx.
Lang lang = new Lang(code: Locale.FRENCH.language, supported: true).save(flush: true)
def langProperties = lang.properties
assert properties
println langProperties.getClass().canonicalName
println langProperties.toMapString()
println Lang.findAll().toListString(Lang.count)
