;;; Pallet project configuration file

(require
 '[delivery.groups.delivery :refer [delivery]])

(defproject delivery
  :provider {:jclouds
             {:node-spec
              {:image {:os-family :ubuntu :os-version-matches "12.04"
                       :os-64-bit true}}}}

  :groups [delivery])
